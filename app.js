/**
 * App() 必须在 app.js 中注册，不能注册多个。
 * 不要在定义于 App() 内的函数中调用 getApp() ，使用 this 就可以拿到 app 实例。
 * 不要在 onLaunch 的时候调用 getCurrentPages()，此时 page 还没有生成。
 * 通过 getApp() 获取实例之后，不要私自调用生命周期函数。
 */
App({
  // 生命周期函数--监听小程序初始化
  onLaunch: function (options) {
    wx.clearStorageSync()
    wx.setEnableDebug({
      enableDebug: false
    })
  },
  // 生命周期函数--监听小程序显示
  onShow: function (options){
    // console.log('小程序显示')
  },
  // 生命周期函数--监听小程序隐藏
  onHide: function(){
    // console.log('小程序隐藏')
  },
  // 错误监听函数
  onError: function (msg) {
    console.log('方法错误', msg)
  },
  // 页面不存在监听函数
  onPageNotFound: function () {
    console.log('方法不存在', msg)
  },
  // 以下:开发者可以添加任意的函数或数据到 Object 参数中，用 this 可以访问, 
  globalData: {
    url: 'https://newapi.autocloudpro.com/',    // 测试库
    // url: 'https://uatxcx.autocloudpro.com/',    // UAT
    // url: 'https://xcx.autocloudpro.com/',       // 正式库
    userInfo: {
      newShoppingCart: 0,
      newOrder: 0,
      newIOU: 0,
      newBalance: 0,
      queryNumber: 0,
      headimgurl: '',
      phone: '',
      zhizhao: {
        status: false,
        images: '',
        company: '',
        contact: '',
        phone: ''
      }
    },
    SERVICE_CONFIG: {
      siteid: 'dx_1000'    // 正式
      // siteid: 'cx_1000'       // 测试
    }
  }
})