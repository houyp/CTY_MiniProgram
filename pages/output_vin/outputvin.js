const common = require('../../public/js/common.js')
const app = getApp()
const regeneratorRuntime = require('../../public/js/regenerator-runtime')
import md5 from '../../public/js/md5.js'
const recorderManager = wx.getRecorderManager()
let LogKey
let guid
let requestTask = wx.request({
  url: 'http://',
  header: {
    'content-type': 'application/json'
  },
  success: function (res) {
    // console.log(res.data)
  }
})

Page({
  data: {
    userAttr: null,                                  // 用户属性 1 普通用户 2 太 3 太T 4阳T  5阳
    height: null,                                    // 高度
    voiceLock: true,                                 // 录音控件锁
    vin: '',                                         // 查询的VIN
    isResult: false,                                 // 是否有查询结果
    isOnly: true,                                    // EPC查询结果是否唯一
    VINData: {},                                     // 查询的小程序数据
    VINDateEPC: { 'content': {},'diff_list': []},    // 查询的EPC数据（EPC查无数据时，会返回小程序数据）
    addAddressData: {
      area: {
        area: '收货区',
        city: '收货市',
        province: '收货省',
        areaid: null,
        cityid: null,
        provinceid: null,
        area_id: null,
        city_id: null,
        province_id: null
      }
    },
    addressSelect: {                                  // 选择的下拉框
      isShow: false,                                  // 是否显示
      chooseType: null,                               // 当前下拉显示的类型
      list: []                                        // 当前下拉显示的列表
    },
    addressList: [],                                  // 地址列表
    partList: [{
      isShow: false,                                  // 查询配件是否有结果
      isfocus: false,                                 // 控制input的焦点，def为false，无焦点
      inputString: '',
      searchString: '',                               // 搜索关键词
      searchTime: 0,                                  // 搜索时间
      searchResult: [],                               // 根据关键词查到的标准配件名称
      partResult:[]                                   // 根据标准配件名称查到的具体配件列表
    }],
    voice: {
      isShow: false,                                  // 选择录音类型 
      current: -1,                                    // 选好的类型的下标
      typeList: [{                                    // 录音类型
        value: 'sms16k',
        language: '普通话'
      },{
        value: 'cantonese16k',
        language: '粤语'
      }]
    },
    voiceBox: {
      voiceBoxShow: false,                            // 录音框是否显示
      voiceIndex: null                                // 所录的配件的下标
    },
    timer: null,                                      // 定时器
    toView: null,                                     // 有结果后的定位（通过下标写入CLASS）
    dialog: {                                         // 弹窗内容
      show: false,
      plateNo: null,    // 车牌号
      insuranceNo: null,  // 备案号
      addressList: [],
      addressIndex: null,
      isTax: 1,          // 是否开发票：0-不开发票，1-开发票
      note: null,
    }
  },

  onLoad: function (options) {
    guid = wx.getStorageSync('guid') || ''
    const Announcement = options.Announcement || ''
    const that = this
    const QueryVINData = wx.getStorageSync('QueryVINData')
    let updateDate = {
      'vin': wx.getStorageSync('Last_Search_VIN') || '',
      'userAttr': app.globalData.userInfo.userAttr
    }
    if (JSON.stringify(QueryVINData.CTV_DATA) === '{}' && JSON.stringify(QueryVINData.EPC_DATA) === '{}') {
      updateDate['isOnly'] = false
      updateDate['isResult'] = false
    } else {
      const appletVin = QueryVINData.CTV_DATA
      const epcVins = QueryVINData.EPC_DATA
      updateDate['VINData'] = appletVin.listCarModel && appletVin.listCarModel.length > 0 
                              ? this.dealwithData(appletVin.listCarModel, Announcement) : {}
      updateDate['VINDateEPC'] = JSON.stringify(epcVins) !== '{}'
                                  ? epcVins.listCarModel : {}
      updateDate['isOnly'] = JSON.stringify(epcVins) !== '{}' && epcVins.listCarModel.diff_list.length === 0 
                              ? true : false
      updateDate['isResult'] = true
    }
    this.setData(updateDate)
    wx.getSystemInfo({
      success: function (res) {
        that.setData({ height: res.windowHeight})
      }
    })
  },
  onShow: function () {
    wx.setStorageSync('inqueryData', {})
    // 每次进来时，如果省市区为空就帮用户填上一个订单的省市区
    if (!this.data.addAddressData.area.provinceid || !this.data.addAddressData.area.cityid || !this.data.addAddressData.area.areaid) {
      let that = this
      wx.request({
        url: app.globalData.url + 'Conter/UpOrderAddress',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        data: {
          'uid': guid
        },
        success: function (res) {
          if (res.data.code === 1) {
            let province_id = res.data.data.province_id
            let city_id = res.data.data.city_id
            let area_id = res.data.data.area_id || ''
            if (!common.getAddressName(area_id, '2')) { // 如果区的信息为空，通过市ID获取数据进入帮选阶段
              const AreaList = common.getAreaList(city_id)
              if (AreaList.length == 1) {   // 区数据唯一，帮选
                area_id = AreaList[0].id
              } else {                      // 否则弹出列表
                that.setData({addressSelect: {
                  isShow: true,
                  chooseType: 'area',
                  list: common.getAreaList(city_id)
                }})
              }
            }
            that.setData({addAddressData: {area: {
              area: common.getAddressName(area_id, '2') ? common.getAddressName(area_id, '2') : '收货区',
              city: common.getAddressName(city_id, '1'),
              province: common.getAddressName(province_id, '0'),
              areaid: area_id,
              cityid: city_id,
              provinceid: province_id,
              area_id: area_id,
              city_id: city_id,
              province_id: province_id,
            }}})
          }
        }
      })
    }
  },
  onUnload: function () {
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  // 处理vin的查询数据，得到字段值统一的数据, Announcement为选择的公告号
  dealwithData: function (CTV_DATA_Array, Announcement) {
    let rawData = Announcement 
          ? CTV_DATA_Array.filter((item) => {
              return item['公告号'] === Announcement
            }) 
          : CTV_DATA_Array
    if (rawData.length === 1) {
      return rawData[0]
    } else {
      let obj = rawData[0]
      for (let i = 0; i < rawData.length; i++) {
        Object.keys(rawData[i]).forEach((key) => {
          if (obj[key] !== rawData[i][key]) {
            delete obj[key]
          }
        })
      }
      return obj
    }
  },

  /**
   * 开启波纹
   */
  rippleStart: function () {
    this.rippleEnd()
    let that = this
    let context = wx.createCanvasContext('voice-canvas')
    let width = wx.getSystemInfoSync().windowWidth
    let height = null
    var query = wx.createSelectorQuery();
    query.select('#voice-box').boundingClientRect()
    query.exec(function (res) {
      height = (res[0].height) / 2
      var arr = [];
      for(var i = 0; i < 1; i++){
          arr.push({x: width/2,y: height});
      };
      var radius = 50;
      var opa = 1
      function Circle (x,y,radius) {
        this.w=x;
        this.h=y;  
        this.radius=radius; 
        this.opa=opa; 
      };
      Circle.prototype.radiu = function(){
        let MaxWidth = 80
        let initWidth = 50
        let Ages = 1
        radius += Ages
        opa = opa - (1 / ((MaxWidth - initWidth) / Ages))
        if (this.radius > MaxWidth) {
          radius = initWidth;
          opa = 1
        };         
      };
      // 绘制圆形的方法
      Circle.prototype.paint=function(){
        context.beginPath();
        context.arc(this.w, this.h, this.radius, 0, Math.PI*2);
        context.closePath();
        context.setLineWidth(1);
        context.setStrokeStyle('rgba(22, 121, 219, ' + this.opa + ')');
        context.stroke()
        context.draw()      
      };  
      // 创建
      var newfun = null;
      function createCircles(){ 
        for(var j=0;j<arr.length;j++){         
            newfun= new Circle(arr[j].x,arr[j].y,radius);
            newfun.paint();
        }; 
        newfun.radiu();  
      };
      var render = function() {
        createCircles();
      };
      that.timer = setInterval(function(){render()}, 50)
    })
  },
  /**
   * 关闭波纹
   */
  rippleEnd: function () {
    clearInterval(this.timer)
    this.timer = null
    let context = wx.createCanvasContext('voice-canvas')
    context.clearRect(0 ,0 ,wx.getSystemInfoSync().windowWidth, wx.getSystemInfoSync().windowHeight)
    context.draw()
  },
  /**
   * 关闭录音框
   */
  closeVoiceBox: function (e) {
    this.rippleEnd(0)
    this.setData({voiceBox: {
      voiceBoxShow: false,
      voiceIndex: null
    }})
  },

  /**
   * 选择省份
   */
  chooseProvince: function (e) {
    this.setData({'addressSelect': {
      isShow: true,
      chooseType: 'province',
      list: common.getProvinceList()
    }})
  },
  /**
   * 选择市
   */
  chooseCity: function(e) {
    if (this.data.addAddressData.provinceid === 0) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请先选择收货省份',
      })
      return false
    }
    let _id = e.currentTarget.dataset._id
    this.setData({
      'addressSelect': {
        isShow: true,
        chooseType: 'city',
        list: common.getCityList(_id)
      }
    })
  },
  /**
   * 选择区
   */
  chooseArea: function(e) {
    if (this.data.addAddressData.provinceid === 0 || this.data.addAddressData.cityid === 0) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请先选择收货省市',
      })
      return false
    }
    let _id = e.currentTarget.dataset._id
    this.setData({
      'addressSelect': {
        isShow: true,
        chooseType: 'area',
        list: common.getAreaList(_id)
      }
    })
  },
  
  /**
   * 选择地址下拉
   */
  chooseAreaList: function (e) {
    let id = e.currentTarget.dataset.id
    let _id = e.currentTarget.dataset._id
    let name = e.currentTarget.dataset.name
    let addAddressData = this.data.addAddressData
    switch (this.data.addressSelect.chooseType) {
      case 'province':
        addAddressData.area.province = name
        addAddressData.area.provinceid = id
        addAddressData.area.province_id = _id
        addAddressData.area.city = '收货市'
        addAddressData.area.cityid = null
        addAddressData.area.city_id = null
        addAddressData.area.area = '收货区'
        addAddressData.area.areaid = null
        addAddressData.area.area_id = null
        this.setData({ 'addAddressData': addAddressData})
        this.chooseCity(e)
        break
      case 'city':
        addAddressData.area.city = name
        addAddressData.area.cityid = id
        addAddressData.area.city_id = _id
        addAddressData.area.area = '收货区'
        addAddressData.area.areaid = null
        addAddressData.area.area_id = null
        this.setData({ 'addAddressData': addAddressData })
        this.chooseArea(e)
        break
      case 'area':
        addAddressData.area.area = name
        addAddressData.area.areaid = id
        addAddressData.area.area_id = _id
        this.setData({ 'addAddressData': addAddressData, 'partList': [{
          isShow: false,
          isfocus: false, 
          inputString: '',
          searchString: '',
          searchTime: 0,
          searchResult: [], 
          partResult:[]   
        }]})
        this.hideSelectList()
        break
      default:
        break
    }
  },
  /**
   * 关闭地址下拉
   */
  hideSelectList: function () {
    this.setData({
      'addressSelect': {
        isShow: false,
        chooseType: null,
        list: []
      }
    })
  },
  /**
   * 删除配件
   */
  deletePart: function(e) {
    let partList = this.data.partList
    partList.splice(e.currentTarget.dataset.index, 1)
    this.setData({ 'partList': partList })
    // this.pushNewPartData()
  },
  /**
   * 更新搜索的字符串
   */
  setSearchString: function(e) {
    let self = this
    let partList = this.data.partList
    let oldinputString = partList[e.currentTarget.dataset.index].inputString
    partList[e.currentTarget.dataset.index].inputString = e.detail.value.trim().toUpperCase().replace(/[^\u4e00-\u9fa5a-zA-Z0-9\s，,-]/g, '')
    partList[e.currentTarget.dataset.index].searchTime = new Date().getTime()
    this.setData({ 'partList': partList })
    if (oldinputString != partList[e.currentTarget.dataset.index].inputString) {
      self.getPartData(partList[e.currentTarget.dataset.index].inputString, e.currentTarget.dataset.index)
    }
  },
  /**
   * 收起下拉, 搜索词和搜索列表匹配，false:收起，true:点击匹配到的
   */
  searchhideChoose: function(e, indexs, oe) {
    let index = e ? e.currentTarget.dataset.index : indexs
    let indexSearch = null
    let partList = this.data.partList
    let isHas = false
    partList[index].searchResult.forEach((searchPart, indexPart) => {
      if (searchPart.name == partList[index].inputString) {
        indexSearch = indexPart
        isHas = true
      }
    })
    if (isHas) {
      // list存在相同名称的配件，帮选
      this.searchfinishChoose(null, index, indexSearch, null, null)
    } else {
      // 不存在，直接赋值，并通知日志
      let that = this
      let partname = partList[index].searchString ? partList[index].searchString : partList[index].inputString
      partList[index].isShow = true
      partList[index].isfocus = false
      partList[index].searchResult = []
      partList[index].partResult = [{ 
        'name': partname,
        'oe': oe,
        'number': 1
      }]
      wx.request({
        url: app.globalData.url + 'Api/getPartsLog',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        data: {
          'partname': partname,
          'partoe': '',
          'vin': that.data.vin,
          'guid': guid,
          'brandname': that.data.VINData['subbrand'],
          'key': LogKey
        },
        success: (response) => {
          
        }
      })
      this.scroll()
      this.pushNewPartData()
      this.setData({ 'partList': partList })
    }
  },
  /**
   * 点击搜索列表
   */
  searchfinishChoose: function (e, indexs, indexSearchs, strSearch, OESearch) {
    // 点击：利用e定位
    let partList = this.data.partList
    let serachNameIndex = e ? e.currentTarget.dataset.index : indexs
    let indexSearch = e ? e.currentTarget.dataset.indexsearch : indexSearchs
    let searchString = partList[serachNameIndex].searchResult.length > 0
                        ? partList[serachNameIndex].searchResult[indexSearch].name : strSearch
    let serachOE = partList[serachNameIndex].searchResult.length > 0
                    ? partList[serachNameIndex].searchResult[indexSearch].openVoiceBox : OESearch

    partList[serachNameIndex].isfocus = false
    partList[serachNameIndex].searchString = searchString
    partList[serachNameIndex].searchTime = new Date().getTime()
    partList[serachNameIndex].searchResult = []

    this.getPartAjax(serachNameIndex, searchString, serachOE, partList)
  },
  /**
   * 发起配件信息搜索的方法
   * @param {Number} searchIndex 搜索的配件下标
   * @param {String} searchString 搜索的配件名称
   * @param {String} serachOE 搜索的配件OE
   * @param {Array} partList 配件列表
   */
  getPartAjax: function (searchIndex, searchString, serachOE, partList) {
    if (this.data.isOnly) {   // 有EPC数据 且精确到唯一车型
      wx.showLoading({
        title: '数据加载中...',
        mask: true
      })
      // 请求EPC配件数据
      this.getPartMessage(searchString, searchIndex, serachOE)
    } else {
      if (this.data.VINData['公告号']) {
        wx.showLoading({
          title: '数据加载中...',
          mask: true
        })
        // 请求J库数据
        this.getPartMessageByJ(searchString, searchIndex, serachOE)
      } else {
        // 无EPC数据且无公告号. 直接赋到搜索结果
        var that = this
        partList[searchIndex].partResult = [{ 'name': searchString, 'oe': serachOE, 'number': 1 }]
        wx.request({
          url: app.globalData.url + 'Api/getPartsLog',
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          data: {
            'partname': searchString,
            'partoe': serachOE,
            'vin': that.data.vin,
            'guid': guid,
            'brandname': that.data.VINData['subbrand'],
            'key': LogKey
          },
          success: (response) => {
            
          }
        })
        this.pushNewPartData()
      }
    }
    // 统一赋值
    this.setData({ 'partList': partList, 'toView': null })
  },
  /**
   * 查询EPC配件数据
   * @param {String} searchString 搜索词
   * @param {Number} serachNameIndex 搜索下标
   * @param {String} serachOE 搜索OE
   */
  getPartMessage: function (searchString, serachNameIndex, serachOE) {
    let partList = this.data.partList
    let that = this
    wx.request({
      url: app.globalData.url + 'Api/getParts',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        'parts': searchString,
        'vin': that.data.vin,
        'brandname': that.data.VINData['subbrand'],
        'guid': guid,
        'key': LogKey
      },
      success: (response) => {
        let res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
        if (res.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.code == 12) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.message,
          })
          return ''
        }
        if (res.code === 1 && res.data.length > 0) {
          let partData = res.data
          that.getPartPriceList(partData, that.data.addAddressData.area.provinceid, that.data.addAddressData.area.cityid, that.data.addAddressData.area.areaid, serachNameIndex)
        } else {
          partList[serachNameIndex].isShow = true
          partList[serachNameIndex].partResult = [{ name: searchString, oe: serachOE, number: 1}]
          that.setData({ 'partList': partList})
          that.pushNewPartData()
          wx.hideLoading()
        }
      }
    })
  },
  /**
   * 查询J库配件数据
   */
  getPartMessageByJ: function (searchString, serachNameIndex, serachOE) {
    let that = this
    let partList = this.data.partList
    wx.request({
      url: app.globalData.url + 'Api/getPartsByJ',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        'vin': that.data.vin,
        'uid': guid,
        'part_name': searchString,
        'province': that.data.addAddressData.area.provinceid,
        'city': that.data.addAddressData.area.cityid,
        'areaid': that.data.addAddressData.area.areaid,
        'notif_num': that.data.VINData['公告号'],
        'year': that.data.VINData['生产年份'] ? that.data.VINData['生产年份'] : '',
        'brandname': that.data.VINData['subbrand'],
        'key': LogKey
      },
      success: function (response) {
        let res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
        if (res.code === 1 && res.data.length > 0) {
          let partData = res.data
          that.getPartPriceList(partData, that.data.addAddressData.area.provinceid, that.data.addAddressData.area.cityid, that.data.addAddressData.area.areaid, serachNameIndex)
        } else {
          partList[serachNameIndex].isShow = true
          partList[serachNameIndex].partResult = [{ name: searchString, oe: serachOE}]
          that.setData({ 'partList': partList})
          that.pushNewPartData()
          wx.hideLoading()
        }
      }
    })
  },
  /**
   * 获取配件价格列表，并进行组合拼装
   */
  getPartPriceList: function (partData, ProvinceId, CityId, Areaid, serachNameIndex) {
    let IS_POST_DATA = true
    let Robj = []
    let that = this
    setTimeout(function () {
      if (IS_POST_DATA) {
        wx.showLoading({
          title: '请耐心等待...',
          mask: true
        })
      }
    }, 2000)
    let allEpcPartsCodes = []
    partData.forEach((key) => {
      allEpcPartsCodes.push(key.PartCode)
    })
    partData.forEach((key, index) => {
      let NewTime = new Date().getTime()
      let Sign = md5('PicPath=' + partData[index].PicPath + '&Time=' + NewTime + '&key=uC4LtsWobTqwQ83i')
      let NewObj = {
        'name': partData[index].PartName_CN ? partData[index].PartName_CN : '',
        'oe': partData[index].PartCode ? partData[index].PartCode : '',
        'area': partData[index].PartArea ? partData[index].PartArea : '',
        'number': 1,
        'note': partData[index].note ? ''.concat(...partData[index].note) : '',
        'rawNote': partData[index].note,
        'allEpcPartsCodes': allEpcPartsCodes,
        'priceList': [],
        'defaultPrice': {},
        'priceListShow': false
      }
      NewObj.url = partData[index].PicPath 
      ? [{ 
          'url': partData[index].PicPath ? app.globalData.url + 'Api/getPartsIMG?PicPath=' + partData[index].PicPath + '&Time=' + NewTime + '&Sign=' + Sign + '' : '',
          'path': partData[index].PicPath ? partData[index].PicPath : '', 
          'name': partData[index].PicName ? partData[index].PicName : '', 
          'area': partData[index].PartArea ? partData[index].PartArea : ''
        }] 
      : []
      if (partData[index].PartCode) {
        wx.request({
          url: app.globalData.url + 'Query/speedInquiry',
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          data: {
            'vin': this.data.vin,
            'partsName': partData[index].PartName_CN,
            'partsCode': partData[index].PartCode,
            'provinceId': ProvinceId,
            'cityId': CityId,
            'districtId': Areaid,
            'townId': '',
            'userId': guid,
            'userType': '',
            'carBrandId': this.data.VINData['厂牌编号'],
            'carSystemId': this.data.VINData['车系编号']
          },
          success: (res) => {
            wx.hideLoading()
            if (res.data.code == 0) {
              wx.reLaunch({
                url: '/pages/authorization/authorization',
              })
              return
            }
            if (res.data.code == 12) {
              wx.showModal({
                title: '提示',
                showCancel: false,
                content: res.data.message,
              })
              return ''
            }
            if (res.data.code === 1 && !res.data.data.empty) {
              let PriceList = res.data.data.model.sort(function(a, b){
                return a.salePriceWithTax > b.salePriceWithTax
              })
              NewObj.priceList = PriceList
              NewObj.defaultPrice = PriceList[0]
              NewObj.priceListShow = false
            }
            Robj[index] = NewObj
            verifyAllParts()
          }
        })
      } else {
        Robj[index] = NewObj
      }
      const verifyAllParts = function () {
        if (Robj.length === partData.length) {
          let partList = that.data.partList
          partList[serachNameIndex].isShow = false
          partList[serachNameIndex].partResult = Robj
          that.setData({ 'partList': partList })
          that.pushNewPartData()
          IS_POST_DATA = false
          wx.hideLoading()
        }
      }
    })
  },
  /**
   * 点击输入框
   */
  focusSearchString: function (e) {
    if (this.data.addAddressData.areaid == 0 || this.data.addressSelect.isShow) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请先选择收货地址',
      })
      return ''
    }
    let index = e.currentTarget.dataset.index
    let partList = this.data.partList
    partList[index].isfocus = true
    this.setData({ partList: partList})
  },
  /**
   * 移出输入框，且VIN结果不唯一
   */
  blurSearchString: function(e, indexs) {
    let that = this
    let index = e ? e.currentTarget.dataset.index : indexs
    let inputString = this.data.partList[index].inputString
    if (inputString != '') {       //有搜索词
      //	如果中间有空格将字体拆成数组
			let arr = inputString.replace(/\s/g, ',');
          arr = arr.replace(/，/g, ',');
          arr = arr.split(',');
      let partList = this.data.partList
      let update
      if (arr.length == 1) {   // 是否有空格间隔
        if (partList[index].searchResult.length == 0) {       // 没有间隔，且没有关联词,就直接显示
          that.searchhideChoose(e, indexs)
        }
        partList[index].isfocus = false
        update = partList
      } else {   // 有间隔，分割开
        let oldListLen = partList.length
        let NewData = []
        arr.filter(function (item) {
          return !!item
        }).forEach((key, index) => {
          NewData.push({
            isShow: true,
            isfocus: false,
            searchString: key,
            inputString: key,
            searchTime: new Date().getTime(),
            searchResult: [],
            partResult: [{'name': key, 'number': 1}]
          })
          setTimeout(function(){
            that.searchfinishChoose(null, oldListLen + index - 1, null, key, null)
          }, 300)
        })
        update = partList.slice(0, index).concat(NewData)
      }
      this.setData({ partList: update })
    }
    that.pushNewPartData()
  },
  /**
   * 发送搜索请求,返回配件信息和价格
   */
  getPartData: function (searchString, index) {
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    requestTask.abort()
    requestTask = wx.request({
      url: app.globalData.url + 'Api/FuzzySearch',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        'key': searchString, // 默认值
        'guid': guid,
        'vin': this.data.vin,
        'provinceid': this.data.addAddressData.area.provinceid,
        'cityid': this.data.addAddressData.area.cityid,
        'areaid': this.data.addAddressData.area.areaid,
        'brandname': this.data.VINData['subbrand']
      },
      success: (res) => {
        wx.hideLoading()
        LogKey = res.data.key
        if (res.data.code === 1) {
          that.getDataAfterOpenSeclet(res.data.data, index)
        } else if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        } else if (res.data.code == 404) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '请上传营业执照等信息，进行后续流程，点击确定进入上传界面',
            success: function (response) {
              if (response.confirm) {
                wx.reLaunch({
                  url: '/pages/enterprise_information/centerpriseinfomation',
                })
              } else if (response.cancel) {

              }
            }
          })
          return
        } else {
          let partList = that.data.partList
          partList[index].searchResult = []
          that.setData({ 'partList': partList })
          if (wx.getStorageSync('searchType') == 'voice') {
            that.pushNewPartData()
            that.blurSearchString(null, index)
            wx.setStorageSync('searchType', '')
          }
        }
      }
    })
  },
  /**
   * 拿到配件列表数据的操作
   */
  getDataAfterOpenSeclet:function (res, index) {
    let partList = this.data.partList
    let obj = []
    res.forEach((key, ind) => {
      let objModel = {}
      let hotWord = Number(res[ind].propor || 0)
      objModel.name = res[ind].part_name
      objModel.num = res[ind].num
      objModel.oe = res[ind].oe
      objModel.propor = hotWord
      if (0.2 < hotWord) {
        objModel.className = 'one'
      } else if (0.05 < hotWord) {
        objModel.className = 'two'
      } else if (0 <= hotWord) {
        objModel.className = 'three'
      } else {
        objModel.className = ''
      }
      obj.push(objModel)
    })
    partList[index].searchResult = obj
    this.setData({ 'partList': partList, 'toView': 'part-' + index })
  },
  /**
   * 结果大于1个的时候，选择精确到一个
   */
  chooseOE: function (e, ind, indchild) {
    let index
    let indexChild
    if (e) {
      index = e.currentTarget.dataset.index
      indexChild = e.currentTarget.dataset.indexchild
    } else {
      index = ind
      indexChild = indchild
    }
    let partList = this.data.partList
    let PartResult = this.data.partList[index].partResult[indexChild]
    partList[index].partResult = [PartResult]
    partList[index].isShow = true
    this.pushNewPartData()
    this.setData({
      'partList': partList
    })
  },
  /**
   * 关闭多个配件结果列表
   */
  closeSearchList: function (e) {
    let index = e.currentTarget.dataset.index
    let partList = this.data.partList
    partList[index].isShow = true
    partList[index].partResult = [{ 'name': partList[index].searchString, 'number': 1 }]
    this.setData({ 'partList': partList})
    this.pushNewPartData()
  },
  /**
   * 查看装配图
   */
  checkImage: function (e) {
    this.getImageArea(e, e.currentTarget.dataset.urlarray)
  },
  /**
   * 装配图的位置高亮
   */
  getImageArea: function (e, urlArray) {
    let that = this
    let imageArray = []
    let ArrLen = 0
    let urlArrayLen = urlArray.length
    urlArray.forEach((key) => {
      let obj = {
        'url': encodeURIComponent(key.url)
      }
      wx.request({
        url: app.globalData.url + '',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        data: {
          'path': key.path,
          'area': key.area,
          'guid': guid
        },
        success: function (res) {
          if (res.data.code == 0) {
            wx.reLaunch({
              url: '/pages/authorization/authorization',
            })
            return
          }
          if (res.data.code == 12) {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: res.data.message,
            })
            return ''
          }
          if (res.data.code == 1) {
            obj.area = res.data.data
          }
          imageArray.push(obj)
          if (++ArrLen == urlArrayLen) {
            that.showImageSwiper(e, imageArray)
          }
        }
      })
    })
  },
  /**
   * 显示装配图轮播
   */
  showImageSwiper: function (e, imageArray) {
    let index = e.currentTarget.dataset.index
    let indexChild = e.currentTarget.dataset.indexchild
    let params = JSON.stringify(imageArray)
    wx.navigateTo({ url: '/pages/assembly_drawing/assemblydrawing?imagearray=' + params + '' })   
  },
  /**
   * 配件列表是否新增
   */
  pushNewPartData: function () {
    let partList = this.data.partList
    var len = partList.length
    while(len--) {
      if (!partList[len].inputString) {
        partList.splice(len, 1)
      }
    }
    partList.push({
      isShow: false,
      isfocus: false,
      searchString: '',
      inputString: '',
      searchTime: 0,
      searchResult: [],
      partResult: []
    })
    this.setData({'partList': partList})
  },
  /**
   * 滚动时触发
   */
  scroll: function (e) {
    let partList = this.data.partList
    partList.forEach((part) => {
      part.isfocus = false
    })
    this.setData({ 'partList': partList})
  },
  /**
   * 再次编辑
   */
  againChangeSearch: function (e, index) {
    let indexs = e ? e.currentTarget.dataset.index : index
    let partList = this.data.partList
    partList[indexs].isShow = false
    partList[indexs].isfocus = true
    partList[indexs].searchString = ''
    partList[indexs].searchTime = new Date().getTime()
    partList[indexs].partResult = []
    this.setData({ partList: partList, 'toView': 'part-' + indexs})
    this.getPartData(partList[indexs].inputString, indexs)
  },
  /**
   * 打开录音弹出框
   */
  openVoiceBox: function (e) {
    let that = this
    let index = e.currentTarget.dataset.index
    if (this.data.addAddressData.areaid == 0 || this.data.addressSelect.isShow) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请先选择收货地址',
      })
      return false
    }
    if (this.data.voice.current == -1) {
      this.chooseVoiceType(e)
      return
    }
    this.setData({voiceBox: {
      voiceBoxShow: true,
      voiceIndex: index
    }})
  },
  /**
   * 选择录音的种类
   */
  chooseVoiceType: function (e) {
    let that = this
    wx.showActionSheet({
      itemList: ['普通话', '粤语'],
      success: function(res) {
        let voice = that.data.voice
        voice.isShow = true
        voice.current = res.tapIndex
        that.setData({voice: voice})
        if (e.target.dataset.hasOwnProperty('index')) {
          that.openVoiceBox(e)
        }
      },
      fail: function(res) {
        //console.log(res.errMsg) // 关闭语言选择
      }
    })
  },
  /**
   * 按下录音搜索
   */
  touchstartSearchVoice: function() {
    let that = this
    this.setData({ 'voiceLock': true })
    recorderManager.onStart(() => {
      that.rippleStart()
    })
    const options = {
      duration: 5000,
      sampleRate: 44100,
      numberOfChannels: 1,
      encodeBitRate: 192000,
      format: 'mp3',
      frameSize: 50
    }
    wx.authorize({
      scope: 'scope.record',
      success(res) {
        if (that.data.voiceLock) {
          recorderManager.start(options)
          wx.setStorageSync('recorderStartDate', new Date().getTime())
        } 
      },
      fail() {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: '未授权录音，功能将无法使用，点击重新获取',
          success: function () {
            wx.openSetting({
              success: function(res) {
              }
            })
          }
        })
      }
    })
  },
  /**
   * 录音被打断
   */
  touchcancelSearchVoice: function (e) {
    let that = this
    let index = e.currentTarget.dataset.index
    const recorderManager = wx.getRecorderManager()
    recorderManager.stop()
    recorderManager.onStop((res) => {
      that.rippleEnd()
      that.setData({ 'voiceLock': false })
    })
  },
  /**
   * 松开录音搜索按钮
   */
  touchendSearchVoice: function (e) {
    let that = this
    let index = e.currentTarget.dataset.index
    this.setData({ 'voiceLock': false })
    recorderManager.stop()
    recorderManager.onStop((res) => {
      that.rippleEnd()
      let afterDate = wx.getStorageSync('recorderStartDate')
      let currentDate = new Date().getTime()
      if (currentDate - afterDate < 500) {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: '录音时长太短',
        })
      } else {
        that.fileToText(res.tempFilePath, index)
      }
    })
  },
  /**
   * 科大讯飞，语音听写，识别转成文字
   */
  fileToText: function (filepath, index) {
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.uploadFile({
      url: app.globalData.url + 'Api/iconIdentify',
      filePath: filepath,
      name: 'audioFile',
      formData: {
        'type': this.data.voice.typeList[this.data.voice.current].value,
      },
      success: (res) => {
        wx.hideLoading()
        if (JSON.parse(res.data).code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (JSON.parse(res.data).code == 12) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: JSON.parse(res.data).message,
          })
          return ''
        }
        if (JSON.parse(res.data).code === 1) {
          that.handleData(JSON.parse(res.data).data, index)
          wx.setStorageSync('searchType', 'voice')
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '识别失败，请重新识别或手动键入',
          })
        }
        that.setData({voiceBox: {
          voiceBoxShow: false,
          voiceIndex: null
        }})
        that.pushNewPartData()
      }
    })
  },
  /**
   * 拿到返回的文字的处理
   */
  handleData: function (str, index) {
    let partList = this.data.partList
    partList[index].inputString = str
    partList[index].searchTime = new Date().getTime()
    partList[index].partResult = []
    this.setData({ 'partList': partList })
    this.getPartData(str, index)
  },
  /**
   * 打开价格列表
   */
  openPriceList: function (e) {
    let index = e.currentTarget.dataset.index
    let indexChild = e.currentTarget.dataset.indexchild
    let partList = this.data.partList
    partList[index].partResult[0].priceListShow = true
    this.setData({ 'partList': partList})
  },
  /**
   * 关闭价格列表
   */
  closePriceList: function (e) {
    let index = e.currentTarget.dataset.index
    let partList = this.data.partList
    partList[index].partResult[0].priceListShow = false
    this.setData({ 'partList': partList })
  },
  /**
   * 选择价格
   * @param {Object} e
   */
  choosePrice: function (e) {
    let index = e.currentTarget.dataset.index
    let indexPrice = e.currentTarget.dataset.indexprice
    let partList = this.data.partList
    partList[index].partResult[0].defaultPrice = partList[index].partResult[0].priceList[indexPrice]
    this.setData({ 'partList': partList })
    this.closePriceList(e)
  },
  /**
   * 检测询价列表中是否有配件结果
   */
  isSearchResultLength: function () {
    let isBel = true
    let partList = this.data.partList
    partList.forEach((part) => {
      if (part.searchResult.length > 0 || (!part.isShow && part.partResult.length > 1)) {
        isBel = false
      }
    })
    return isBel
  },
  /**
   * 秒报结果加入购物车
   * @param {Object} e
   */
  offerAddCart: function (e) {
    let index = e.currentTarget.dataset.index
    let partList = this.data.partList
    if (partList[index].partResult[0].priceList.length <= 0) {
      return
    }
    if (this.data.addAddressData.areaid == 0 || this.data.addressSelect.isShow) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请先选择收货地址',
      })
      return
    }
    let offterData = []
    let offterObj = {
      "userId": guid,
      "bizId": partList[index].partResult[0].defaultPrice.skuId,
      "partsBrandName": partList[index].partResult[0].defaultPrice.partsBrandName,
      "partsCode": partList[index].partResult[0].oe,
      "partsName": partList[index].partResult[0].name,
      "factoryType": partList[index].partResult[0].defaultPrice.factoryType,
      'factoryTypeName': partList[index].partResult[0].defaultPrice.factoryTypeName,
      "supplierId": partList[index].partResult[0].defaultPrice.supplierId,
      "provinceId": this.data.addAddressData.area.provinceid,
      "cityId": this.data.addAddressData.area.cityid,
      "districtId": this.data.addAddressData.area.areaid,
      "address": '',
      "placeOfOrigin": partList[index].partResult[0].defaultPrice.placeOfOrigin,
      "remark": '',
      "price": partList[index].partResult[0].defaultPrice.salePriceWithTax,
      "nailBoxPrice": partList[index].partResult[0].defaultPrice.nailBoxPrice || 0,   // 新增 by:Chris
      "source": "cx",
      "partsEPCInfo": {
        'partsEpcRemarks': partList[index].partResult[0].rawNote ? partList[index].partResult[0].rawNote : []
      },
      "carTypeDTO": {
        "logoUrl": this.data.VINData['carpic'],
        "vinCode": this.data.vin,
        "brandId": this.data.VINData['品牌编号'],
        "brandName": this.data.VINData['subbrand'],
        "carSystemId": this.data.VINData['车系编号'],
        "carSystemName": this.data.VINData['车系'],
        "carTypeId": null,
        "carType": "",
        "carTypeName": "",
        "engine": "",
        "engineType": this.data.VINData['发动机型号'],
        "engineDisplacement": this.data.VINData['发动机排量(ml)'],
        "engineDescribe": this.data.VINData['发动机描述'],
        "transmission": this.data.VINData['变速箱'],
        "year": this.data.VINData['生产年份'],
        "emissionStandard": this.data.VINData['排放标准'],
        "carBodyStructure": this.data.VINData['车体结构']
      }
    }
    offterData.push(offterObj)
    wx.request({
      url: app.globalData.url + 'Goods/carAddfast',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {data: JSON.stringify(offterData)},
      method: 'POST',
      success: function (res) {
        if (res.data.code == 1 && res.data.data.success) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '加入购物车成功',
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '加入购物车失败，请稍后再试',
          })
        }
      }
    })
  },
  /**
   * 整理加入询价单的参数
   * @returns {{Data: *, inquiryLength: *}}
   */
  addCartData: function () {
    const userAttr = this.data.userAttr
    let inquiryDTO
    // 基础数据参数
    let cartData = {
      'years': this.data.VINData['生产年份'],
      'vinCode': this.data.vin,
      'noticeNo': '',
      'userType': '',
      'contacts': '',
      'phone': '',
      'userId': guid,
      'garageId': guid,
      'remark': '',
      'provinceId': this.data.addAddressData.area.provinceid,
      'cityId': this.data.addAddressData.area.cityid,
      'districtId': this.data.addAddressData.area.areaid,
      'townId': '',
      'source': 'cx',
      'inquiryDetailDTOs': [],
      'carTypeDTO': {
        "brandLogo": this.data.VINData['carpic'],
        'brandId': this.data.VINData['厂牌编号'],
        'brandName': this.data.VINData['subbrand'],
        'carSystemId': this.data.VINData['车系编号'],
        'carSystemName': this.data.VINData['车系'],
        'carTypeId': '',
        'carTypeName': '',
        'interiorColor': '',
        'engine': '',
        'transmission': this.data.VINData['变速箱'],
        'place': '',
        'year': this.data.VINData['生产年份'],
        'level': '',
        'engineDisplacement': this.data.VINData['发动机排量(ml)'],
        'colorCode': ''
      }
    }
    let partList = this.data.partList
    let partListLength = partList.length
    partList.forEach((key, index) => {
      if (partList[index].searchString && partList[index].searchResult.length == 0 && partList[index].partResult.length == 0) {
        this.searchhideChoose(null, index)
      }
      if ((index + 1) < partListLength) {
        if (key.partResult.length == 1) {
          let isBal = true
          cartData['inquiryDetailDTOs'].forEach((keys) => {
            // 有OE的情况下，已有相同OE则不追加；无OE的情况下，已有相同名称则不追加
            if (key.partResult[0].oe && (keys.partsCode == key.partResult[0].oe)) {
              isBal = false
            }
            if (!key.partResult[0].oe && (keys.partsName == key.partResult[0].name)) {
              isBal = false
            }
          })
          if (isBal) {
            cartData['inquiryDetailDTOs'].push({
              'partsName': key.partResult[0].name,
              'partsCode': key.partResult[0].oe,
              'epcPics': key.partResult[0].url ? key.partResult[0].url.map(function(item) {return item.path}) : [],
              'customerPics': [],
              'allEpcPartsCodes': key.partResult[0].allEpcPartsCodes ? key.partResult[0].allEpcPartsCodes.join(',') : '',
              'partsEPCInfo': {
                'partsEpcRemarks': key.partResult[0].rawNote ? key.partResult[0].rawNote : []
              },
              'customerRemark': '',
              'factoryTypes': '1,2',
              'inquiryNum': key.partResult[0].number,
              'area': key.partResult[0].area,
              'price': key.partResult[0].defaultPrice ? key.partResult[0].defaultPrice : ''
            })
          }
        }
      }
    })
    // 2是太保专员  4是阳光保险修理厂
    switch (userAttr) {
      case 2:
      case 4:
        const Dialog = this.data.dialog
        inquiryDTO = {
          inquiryDTO: cartData,
          insuranceDTO: {
            plateNo: Dialog.plateNo || '',
            insuranceNo: Dialog.insuranceNo || '',
            repairDepotId: userAttr == 2 && Dialog.addressIndex ? Dialog.addressList[Dialog.addressIndex].id : '',  //汽修厂id
            isTax: 1,
            insuranceStaffId: userAttr == 4 && Dialog.addressIndex ? Dialog.addressList[Dialog.addressIndex].id : '' // 保险专员ID
          }
        }
        break;
      default:
        inquiryDTO = cartData
        break;
    }
    
    return {Data: inquiryDTO, inquiryLength: cartData['inquiryDetailDTOs'].length}
  },
  /**
   * 
   * @param {Object} e 太保用户 配件数量减少
   */
  reduceNumber: function (e) {
    const index = e.currentTarget.dataset.index
    let partList = this.data.partList
    let currentPart = partList[index].partResult[0]
    currentPart['number'] = Math.max(1, currentPart['number'] - 1)
    this.setData({partList: partList})
  },
  /**
   * 
   * @param {Object} e 太保用户 配件数量增加
   */
  addNumber: function (e) {
    const index = e.currentTarget.dataset.index
    let partList = this.data.partList
    let currentPart = partList[index].partResult[0]
    currentPart['number'] += 1
    this.setData({partList: partList})
  },
  /**
   * 选择修理厂
   * @param {Object} e 
   */
  ChangeDiglogPicker: function (e) {
    let dialog = this.data.dialog
    dialog['addressIndex'] = e.detail.value
    this.setData({dialog: dialog})
  },
  /**
   * 保存保险信息
   */
  submitDiglog: function () {
    let dialog = this.data.dialog
    if (this.isInsuranceMessages()) {
      dialog['show'] = false
      this.setData({dialog: dialog})
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请补全信息后进行保存',
      })
    }
  },
  /**
   * 取消填写保险信息，初始化信息
   */
  clearDiglog: function () {
    this.setData({ dialog: { 
      show: false,
      plateNo: null,
      insuranceNo: null,
      addressList: [],
      addressIndex: null,
      isTax: 1,
      note: null,
    }})
  },
  /**
   * 车牌号
   * @param {Object} e 
   */
  setPlateNo: function (e) {
    let value = e.detail.value.trim().toUpperCase().replace(/[^\u4e00-\u9fa5A-Za-z0-9]/gm, '').slice(0, 7)
    let dialog = this.data.dialog
    dialog['plateNo'] = value
    this.setData({dialog: dialog})
  },
  /**
   * 报案号
   * @param {Object} e 
   */
  setInsuranceNo: function (e) {
    let value = e.detail.value.trim()
    let dialog = this.data.dialog
    dialog['insuranceNo'] = value
    this.setData({dialog: dialog})
  },
  
  /**
   * 检测车牌号、报案号、修理厂是否为空
   */
  isInsuranceMessages: function () {
    const dialogMessages = this.data.dialog
    let isBel = dialogMessages.plateNo && dialogMessages.insuranceNo && dialogMessages.addressIndex && dialogMessages.addressList.length > 0 ? true : false
    return isBel
  },
  /**
   * 初始化弹出
   */
  initDialog: async function () {
    const that = this
    let dialog = this.data.dialog
    dialog['show'] = true
    wx.showLoading({ title: '数据加载中...', mask: true })
    const res = await common.WXRequest('Query/inqureyInsurance', {guid: guid, userType: this.data.userAttr})
    wx.hideLoading()
    if (res.data.code === 1) {
      if (res.data.data.addressList.length == 0) {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: '可供选择的修理厂列表为空，请联系客服',
        })
        return
      }
      let addressListText = []
      res.data.data.addressList.forEach((item) => {
        addressListText.push(item['company'])
      })
      dialog['addressList'] = res.data.data.addressList
      dialog['addressListText'] = addressListText
      dialog['note'] = res.data.data.note
      that.setData({dialog: dialog})
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: res.data.message ? res.data.message : '获取保险修理厂列表及备注失败，请稍后再试',
      })
    }
  },
  /**
   * 全部加入询价单 发布询价
   */
  submitInquiey: function () {
    let that = this
    if (this.data.addAddressData.areaid == 0 || this.data.addressSelect.isShow) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请先选择收货地址',
      })
      return
    }
    if (!this.isSearchResultLength()) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '尚未选择配件，请选择候选列表中的配件',
      })
      return
    }
    if (this.addCartData().inquiryLength == 0) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请先添加配件',
      })
      return
    }
    if (wx.getStorageSync('inqueryData') 
        && wx.getStorageSync('inqueryData') == JSON.stringify(this.addCartData())) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请勿添加重复的配件',
      })
      return
    }
    // 当用户类型是太时，检测是否有保险信息
    if ((this.data.userAttr == 2 || this.data.userAttr == 4) && !this.isInsuranceMessages()) {
      // 开启弹框
      this.initDialog()
      return
    }
    wx.setStorageSync('inqueryData', JSON.stringify(that.addCartData()))
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Query/submitInquiry',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {data: JSON.stringify(this.addCartData().Data), userType: this.data.userAttr},
      method: 'POST',
      success: function (res) {
        wx.hideLoading()
        if (res.data.code === 1) {
          wx.showModal({
            title: '提示',
            cancelText: '再逛逛',
            content: '询价单添加成功，点击确定前往询价单',
            success: function (res) {
              if (res.confirm) {
                wx.reLaunch({
                  url: '/pages/inquiry/inquiry',
                })
              } else if (res.cancel) {
                
              }
            }
          })
        } else if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        } else if (res.data.code === 404) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '请上传营业执照等信息，进行后续流程，点击确定进入上传界面',
            success: function (response) {
              if (response.confirm) {
                wx.reLaunch({
                  url: '/pages/enterprise_information/centerpriseinfomation',
                })
              } else if (response.cancel) {

              }
            }
          })
          return
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '询价单添加失败，请稍后再试',
          })
        }
      }
    })
  }
})