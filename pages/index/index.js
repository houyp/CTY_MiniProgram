
// pages/index/index.js
const app = getApp()
const service = require('../../public/js/service')
const common = require('../../public/js/common.js')
const regeneratorRuntime = require('../../public/js/regenerator-runtime')
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    newShoppingCart: 0,                                         // 新报价
    NoSettlement: 0,                                            // 未结算
    newIOU: 0,                                                  // 白条额度
    newBalance: 0,                                              // 返现余额
    queryNumber: 0,                                             // 查询次数（芸豆）
    headimgurl: '',                                             // 头像图片（弃用）
    isAssets: false,                                            // 白条状态
    userAttr: null,                                                // 用户属性  2:太保
    phone: '',                                                  // 账户电话号
    neworders: {},                                              // 最新的在路上数量和配件信息
    zhizhao: {
      status: false,                                            // 执照状态
      images: '',                                               // 执照照片
      company: '',                                              // 企业名称
      phone: ''                                                 // 电话
    },
    canIUse: wx.canIUse('open-data.type.userNickName')          // 版本库是否支持userNickName
  },

  onLoad: function (e) {
    guid = wx.getStorageSync('guid') || ''
    this.getAnnouncement()
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getIndexMessage()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // console.log('下拉事件')
    // this.getIndexMessage()
    // wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // console.log('上拉触底')
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 获取公告
   */
  getAnnouncement: async function () {
    const res = await common.WXRequest('Index/notice')
    if (res && res.data.code === 1) {
      wx.showModal({
        title: '公告',
        showCancel: false,
        content: res.data.data,
      })
    }
  },
  /**
   * 获取首页信息
   */
  getIndexMessage: async function () {
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    const res = await common.WXRequest('Index/getRecord', {'guid': guid})
    wx.hideLoading()
    if (res.data.code == 0) {
      wx.reLaunch({ url: '/pages/authorization/authorization' })
    } else if (res.data.code == 1) {
      this.setData({
        'newShoppingCart': res.data.data.newprice,
        'NoSettlement': res.data.data.notlement,
        'newIOU': res.data.data.loussurplus,
        'newBalance': res.data.data.cashback,
        'queryNumber': res.data.data.yun,
        'headimgurl': res.data.data.headimgurl,
        'phone': res.data.data.phone,
        'isAssets': res.data.data.isassets,     // 是否有白条
        'userAttr': res.data.data.userAttr,
        'neworders': res.data.data.neworders,
        'zhizhao': {
          'status': (res.data.data.zhizhao && res.data.data.company && res.data.data.contact) ? true : false,
          'images': res.data.data.zhizhao,
          'company': res.data.data.company,
          'phone': res.data.data.contact,
          'shenhe': res.data.data.shenhe
        }
      })
      app.globalData.userInfo = this.data
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: res.data.message ? res.data.message : '获取首页信息失败，请稍后再试',
      })
    }
  },

  /**
   * 跳转到VIN查询
   */
  toInputVin () {
    wx.navigateTo({ url: '/pages/input_vin/inputvin' })
  },
  /**
   * 跳转到购物车
   */
  toShoppingCart() {
    wx.navigateTo({ url: '/pages/shopping_cart/shoppintcart' })
  },
  /**
   * 跳转到订单
   */
  toOrder() {
    wx.navigateTo({ url: '/pages/order/order' })
  },
  /**
   * 跳转到订单
   */
  toInquiry() {
    wx.navigateTo({ url: '/pages/inquiry/inquiry' })
  },
  /**
   * 跳转到资产
   */
  toAssets() {
    wx.navigateTo({ url: '/pages/assets/assets' })
  },
  /**
   * 跳转到企业信息
   */
  toEnterpriseInfomation() {
    wx.navigateTo({ url: '/pages/enterprise_information/centerpriseinfomation' })
  },
  /**
   * 跳转到个人中心
   */
  toCenter(){
    wx.navigateTo({ url: '/pages/center/center' })
  },
  /**
   * 打开客服
   * @param {Object} event 
   */
  OpenService: function (event) {
    const guid = wx.getStorageSync('guid')
    const that = this
    service.getSettingID({
      userid: guid,
      phone: app.globalData.userInfo.phone,
      provinceid: '',
      cityid: '',
      districtid: '',
      orderid: '',
      enquiryid: '',
      cartid: '',
      usertype: '',
      fromtype: ''
    }).then((result) => {
      if (result.data.code == 1) {
        let params = service.createServiceURL({
          siteid: app.globalData.SERVICE_CONFIG.siteid,
          settingid: result.data.data,
          uid: guid,
          uname: app.globalData.userInfo.phone,
          phone: app.globalData.userInfo.phone,
          userlevel: result.data.userlevel
        })
        wx.navigateTo({url: '/pages/service/service?' + params})
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: result.data.message ? result.data.message : '获取客服组失败',
        })
      }
    }).catch((error) => {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: error,
      })
    })
  }

})