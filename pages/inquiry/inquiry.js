import md5 from '../../public/js/md5.js'
const common = require('../../public/js/common.js')
const app = getApp()
const service = require('../../public/js/service')
let Timer;
let guid

Page({

  /**
   * 页面的初始数据模板
   */
  data: {
    userAttr: null,             // 用户类型
    pageIndex: 1,               // 页码
    pageSize: 5,                // 每页显示几个
    searchStatus: false,        // 搜索栏状态
    searchStr: null,            // 搜索关键词
    imgSwiper: {                // 轮播图控件
      show: false,              // 是否展示
      imgUrls:[]                // 展示的URL
    },
    inquiryList: [
      // {             // 询价列表
      //   id: null,                 // 询价单ID
      //   detailStatus: false,      // 询价配件列表显示状态 false不显示
      //   inquiryType: true,        // 询价单状态 是否失效 true未失效
      //   vinCode: null,            // vin
      //   deadline: null,           // 过期时间戳
      //   deadlineDate: '',         // 过期时间戳转化的可读形式
      //   carMessage: {
      //     logo: '',               // 车辆LOGO
      //     brandName: "",          // 品牌名称
      //     carSystemId: null,      // 车系ID
      //     carSystemName: "",      // 车系名称
      //     carTypeId: null,        // 车型ID
      //     carTypeName: "",        // 车型名称
      //     engine: '',             // 发动机  
      //     inquiryId: null,        // 询价单ID
      //     level: "",              // 级别
      //     place: "",              // 地区
      //     transmission: "",       // 变速器
      //     year: "",               // 年款
      //     engineDisplacement: ''  // 发动机排量
      //   },
      //   addressMessage: {
      //     province: '',           // 省份
      //     provinceCode: '000000', // 省份code
      //     city: '',               // 市
      //     cityCode: '000000',     // 市code
      //     area: '',               // 区
      //     areaCode: '000000'      // 区code
      //   },
      //   newQuote: 0,              // 新报价
      //   quoteNumber: 0,           // 已报价
      //   partNumber: 0,            // 配件数
      //   partDetail: [{            // 配件列表
      //     partsName: '',          // 配件名称
      //     partsOE: '',            // 配件OE
      //     epcPictures: [{         // epc图片
      //       inquiryDetailId: null,// 图片ID
      //       picOrder: null,       // 图片顺序
      //       picUrl: ''            // 图片路径
      //     }],
      //     userPictures: [{        // 用户图片
      //       inquiryDetailId: null,
      //       picOrder: null,
      //       picUrl: ''
      //     }],
      //     quoteDetailDTOs: [{     // 该配件的价格列表
      //       factoryType: '',      // 配件类型
      //       supplierName: '',     // 供应商名称
      //       placeOfOrigin: '',    // 产地
      //       remark: '',           // 备注
      //       price: null,          // 金额
      //       supplierPictures: [{  // 供应商传的图
      //         picUrl: '',
      //         quoteDetailId: null,
      //       }],
      //       quoteState: 0         // 配件的询价状态 eg. 0.未加入，1.已加入
      //     }]
      //   }]
      // }
    ]
  },
  onLoad: function () {
    this.setData({userAttr: app.globalData.userInfo.userAttr})
    guid = wx.getStorageSync('guid') || ''
  },
  onShow: function () {
    this.setData({
      searchStatus: false,
      searchStr: null, 
    })
    this.getInquiryData({
      "userId": guid,
      "garageId": null,
      "status": null,
      "pageIndex": 1,
      "pageSize": this.data.pageSize,
      "vinCode": '',
      "inquiryNo": '',
      "userType": ''
    }, 'update')
  },
  onReady: function () {
    this.updatePageDeadline()
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // this.getOrderList()
    // wx.stopPullDownRefresh()
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getInquiryData({
      "userId": guid,
      "garageId": null,
      "status": null,
      "pageIndex": this.data.pageIndex + 1,
      "pageSize": this.data.pageSize,
      "vinCode": '',
      "inquiryNo": '',
      "userType": '',
      "condition": this.data.searchStr ? this.data.searchStr : ''
    }, 'append')
  },
  onUnload: function () {
    clearInterval(Timer)
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  /**
   * 获得搜索框焦点
   */
  focusSearchInput: function (e) {
    this.setData({ searchStatus: true})
  },
  /**
   * 关闭搜索
   */
  closeSearch: function() {
    this.setData({ searchStatus: false, searchStr: null })
    this.getInquiryData({
      "userId": guid,
      "garageId": null,
      "status": null,
      "pageIndex": 1,
      "pageSize": this.data.pageSize,
      "vinCode": '',
      "inquiryNo": '',
      "userType": ''
    }, 'update')
  },
  /**
   * 打开商家图
   */
  openSupplierPictures: function (e) {
    let urls = e.currentTarget.dataset.urlarray
    if (urls.length > 0) {
      this.addImgSwiperData(urls)
      this.openImgSwiper()
    }
  },
  /**
   * 打开装配图和用户图
   */
  openEpcORUserPic: function (e) {
    let EpcPicurls = e.currentTarget.dataset.epcpictures
    let UserPicurls = e.currentTarget.dataset.userpictures
    let allPicurls = EpcPicurls.concat(UserPicurls)
    if (allPicurls.length > 0) {
      this.addImgSwiperData(allPicurls)
      this.openImgSwiper()
    }
  },
  /**
   * 填充轮播图数据
   */
  addImgSwiperData: function (data) {
    let imgSwiper = this.data.imgSwiper
    imgSwiper.imgUrls = data
    this.setData({imgSwiper: imgSwiper})
  },
  /**
   * 打开轮播图
   */
  openImgSwiper: function () {
    let imgSwiper = this.data.imgSwiper
    imgSwiper.show = true
    this.setData({imgSwiper: imgSwiper})
  },
  /**
   * 关闭轮播图
   */
  closeImgSwiper: function () {
    let imgSwiper = this.data.imgSwiper
    imgSwiper.show = false
    imgSwiper.imgUrls = []
    this.setData({imgSwiper: imgSwiper})
  },
  /**
   * 请求询价单列表数据
   */
  getInquiryData: function (datas, handleType) {
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    let that = this
    wx.request({
      url: app.globalData.url + 'Query/inquiryList',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: datas,
      success: (response) => {
        wx.hideLoading()
        let res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
        if (res.code === 1 && !res.data.empty) {
          let inquiryList = this.data.inquiryList
          switch (handleType) {
            case 'append':
              // 数据追加
              inquiryList = inquiryList.concat(that.handleDataToPage(res.data.model.list))
              break;
            case 'update':
              // 数据更新
              inquiryList = that.handleDataToPage(res.data.model.list)
              break;
            default:
              wx.showModal({
                title: '提示',
                showCancel: false,
                content: '请求询价单数据缺少handleType参数',
              })
              break;
          }
          that.setData({inquiryList:inquiryList, pageIndex: !(inquiryList.length % 5) ? inquiryList.length / 5 : parseInt(inquiryList.length / 5) + 1})
        } else if (res.code === 1 && res.data.empty) {
          switch (handleType) {
            case 'append':
              // 数据追加
              break;
            case 'update':
              // 数据更新
              that.setData({inquiryList: [], pageIndex: 1})
              break;
            default:
              wx.showModal({
                title: '提示',
                showCancel: false,
                content: '请求询价单数据缺少handleType参数',
              })
              break;
          }
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.message ? res.message : '询价单数据加载失败，请稍后再试',
          })
        }
      }
    })
  },
  /**
   * 将接口数据处理为页面所需数据格式, important!!!
   */
  handleDataToPage: function (datas) {
    let pageData = []
    datas.forEach((item) => {
      let inquiryObj = {
        id: item.inquiryNo, 
        inquiryTypeRaw: item.inquiryType,
        dataID: item.inquiryId,
        detailStatus: false,
        inquiryType: item.deadline > item.currentTime && item.auditStatus != -1 ? true : false, 
        vinCode: item.vinCode,
        currentTime: item.currentTime,
        deadline: item.deadline,
        auditStatus: item.auditStatus,  // 询价单状态 -1 已取消 0 已失效 1 正常
        deadlineDate: '',
        carMessage: {
          logo: item.carTypeDTO.brandLogo,
          brandName: item.carTypeDTO.brandName,
          carSystemId: item.carTypeDTO.carSystemId,
          carSystemName: item.carTypeDTO.carSystemName,
          carTypeId: item.carTypeDTO.carTypeId,
          carTypeName: item.carTypeDTO.carTypeName,
          engine: item.carTypeDTO.engine,
          inquiryId: item.carTypeDTO.inquiryId,
          level: item.carTypeDTO.level, 
          place: item.carTypeDTO.place,
          transmission: item.carTypeDTO.transmission,
          year: item.carTypeDTO.year,
          engineDisplacement: item.carTypeDTO.engineDisplacement
        },
        addressMessage: {
          province: item.provinceName,
          provinceCode: item.provinceId,
          city: item.cityName, 
          cityCode: item.cityId,
          area: item.districtName,
          areaCode: item.districtId 
        },
        newQuote: item.unreadQuoteCount,
        quoteNumber: item.partsQuoteCount,
        partNumber: item.partsCount,
      }
      pageData.push(inquiryObj)
    })
    return pageData
  },
  /**
   * 将未来的时间戳输出倒计时文本
   */
  deadlineDate: function (deadline, currentTime) {
    let currentDate = currentTime
    let differ = deadline > currentDate ? deadline - currentDate : 0
    let day = parseInt(differ / 86400000)
    let hour = parseInt((differ - 86400000 * day) / 3600000)
    let minute = parseInt((differ - 86400000 * day - 3600000 * hour) / 60000)
    let second = parseInt((differ - 86400000 * day - 3600000 * hour - 60000 * minute) / 1000)
    if (!day && !hour && !minute && !second) {
      return `0秒`
    }
    if (!day && !hour && !minute) {
      return `${second}秒`
    }
    if (!day && !hour) {
      return `${minute}分${second}秒`
    }
    if (!day) {
      return `${hour}时${minute}分${second}秒`
    }
    return `${day}天${hour}时${minute}分${second}秒`
  },
  /**
   * 发起搜索
   */
  searchInquiry: function () {
    if (!this.data.searchStr) {return}
    this.getInquiryData({
      "userId": guid,
      "garageId": null,
      "status": null,
      "pageIndex": 1,
      "pageSize": this.data.pageSize,
      "vinCode": null,
      "inquiryNo": null,
      "userType": '',
      'condition': this.data.searchStr
    }, 'update')
  },
  /**
   * 更改搜索词
   */
  setSearchInput: function (e) {
    this.setData({searchStr: e.detail.value.toUpperCase().replace(/[^a-zA-Z0-9]/ig, '')})
  },
  /**
   * 发起新询价跳转
   */
  toNewInquiry: function () {
    wx.reLaunch({
      url: '/pages/input_vin/inputvin',
    })
  },
  /**
   * 定时器，用来更新页面失效时间倒计时的显示
   */
  updatePageDeadline: function () {
    let that = this
    const interim = function () {
      let inquiryList = that.data.inquiryList
      try {
        inquiryList.forEach((item, index) => {
          inquiryList[index].deadlineDate = that.deadlineDate(item.deadline, item.currentTime)
          if (parseInt(item.deadline/1000) - parseInt(item.currentTime/1000) == 0) {
            // clearInterval(Timer)
            that.getInquiryData({
              "userId": guid,
              "garageId": null,
              "status": null,
              "pageIndex": that.data.pageIndex,
              "pageSize": that.data.pageSize,
              "vinCode": '',
              "inquiryNo": '',
              "userType": ''
            }, 'update')
            inquiryListForeach.break = new Error()
          }
          item.currentTime += 1000
        })
      } catch (error) {
        if (error.message === "inquiryListForeach is not defined") {
          return
        } else throw error
      }
      that.setData({inquiryList: inquiryList})
    }
    interim()
    Timer = setInterval(function() {
      interim()
    }.bind(that), 1000)
  },
  /**
   * 检测过期时间有没有超出当前时间
   */
  detectionDate: function (deadline, currentTime) {
    if (!deadline || !currentTime) {
      return false
    }
    return deadline <= currentTime ? true : false
  },
  /**
   * 展示收起询价单详情
   */
  tabDetail: function (e) {
    let index = e.currentTarget.dataset.index
    let inquiryList = this.data.inquiryList
    let that = this
    if (inquiryList[index].detailStatus) {
      inquiryList[index].detailStatus = false
      inquiryList[index].partDetail = []
      this.setData({ inquiryList: inquiryList})
    } else {
      wx.request({
        url: app.globalData.url + 'Query/inquiryDetail',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        data: {
          userId: guid,
          inquiryNo: inquiryList[index].id,
          inquiryType: inquiryList[index].inquiryTypeRaw
        },
        success: function (response) {
          let res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
          if (res.code == 1 && !res.data.empty) {
            inquiryList[index].detailStatus = true
            inquiryList[index].newQuote = 0        // 新增 by:Chris
            inquiryList[index].totalAmount = that.handleInquiryDetailData(res.data.model.inquiryDetailDTOs).totalAmount
            inquiryList[index].partDetail = that.handleInquiryDetailData(res.data.model.inquiryDetailDTOs).datas
            that.setData({ inquiryList: inquiryList})
          } else if (res.code == 1 && res.data.empty) {
            inquiryList[index].detailStatus = true
            inquiryList[index].newQuote = 0   // 新增 by:Chris
            inquiryList[index].partDetail = []
            that.setData({ inquiryList: inquiryList})
          } else {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: res.message ? res.message : '询价单详情加载失败，请稍后再试',
            })
          }
        }
      })
    }
  },
  /**
   * 将接口返回的询价单详情数据进行处理
   */
  handleInquiryDetailData: function (data) {
    let datas = []
    let totalAmount = 0
    data.forEach((item) => {
      let Part = {
        partsName: item.modifiedPartsName,
        partsOE: item.modifiedPartsCode,
        partsEpcRemarks: item.partsEpcRemarks,
        inquiryNum: item.inquiryNum ? item.inquiryNum : 1,
        partTotalAmount: 0,
        inquiryDetailQuoteStatus: item.inquiryDetailQuoteStatus,
        epcPictures: item.epcPictures ? this.handleEpcPictures(item.epcPictures) : [],
        userPictures: item.userPictures ? this.handleUserPictures(item.userPictures) : [],
        quoteDetailDTOs: []
      }
      item.quoteDetailDTOs && item.quoteDetailDTOs.forEach((price) => {
        let paiceItem = {
          factoryType: price.factoryType,
          partsBrandName: price.partsBrandName,
          supplierName: price.supplierName,
          placeOfOrigin: price.placeOfOrigin,
          remark: price.remark,
          quoteDetailId: price.quoteDetailId,
          price: price.salePriceWithTax,
          nailBoxPrice: price.nailBoxPrice,
          costPrice: price.costPrice ? price.costPrice : 0,
          supplierPictures: price.supplierPictures ? price.supplierPictures : [],
          quoteState: price.inCart ? price.inCart : 0,
          bookOrder: price.bookOrder === 1 ? true : false,
          bookOrderDays: price.bookOrderDays,
          bookAllowReturn: price.bookAllowReturn ===  1 ? '不可' : '可'
        }
        Part.quoteDetailDTOs.push(paiceItem)
        Part.partTotalAmount += (price.nailBoxPrice + price.costPrice + price.salePriceWithTax) * item.inquiryNum
      })
      totalAmount += Part.partTotalAmount
      datas.push(Part)
    })
    return {
      datas: datas,
      totalAmount: totalAmount
    }
  },
  /**
   * 装配图转化数据流
   */
  handleEpcPictures: function (epcPics) {
    let NewTime = new Date().getTime()
    let epcPicsBuf = []
    epcPics.forEach((pic) => {
      let Sign = md5('PicPath=' + pic.picUrl + '&Time=' + NewTime + '&key=uC4LtsWobTqwQ83i')
      let picObj = {
        inquiryDetailPicId: pic.inquiryDetailPicId,
        picOrder: pic.picOrder,
        picType: pic.picType,
        picUrl: app.globalData.url + 'Api/getPartsIMG?PicPath=' + pic.picUrl + '&Time=' + NewTime + '&Sign=' + Sign
      }
      epcPicsBuf.push(picObj)
    })
    return epcPicsBuf
  },
  /**
   * 用户图拼接路径
   */
  handleUserPictures: function (userPics) {
    let UserPicsBuf = []
    userPics.forEach((pic) => {
      let picObj = {
        inquiryDetailPicId: pic.inquiryDetailPicId,
        picOrder: pic.picOrder,
        picType: pic.picType,
        picUrl: 'https://api.autocloudpro.com/' + pic.picUrl 
      }
      UserPicsBuf.push(picObj)
    })
    return UserPicsBuf
  },
  // 加入购物车
  addShoppingCart: function (e) {
    wx.showLoading({
      title: '加入购物车...',
      mask: true
    })
    let that = this
    let index = e.currentTarget.dataset.index
    let indexs = e.currentTarget.dataset.indexs
    let indexPrice = e.currentTarget.dataset.indexprice
    let inquiryList = this.data.inquiryList
    let currentCar = inquiryList[index]
    let currentPrice = inquiryList[index].partDetail[indexs].quoteDetailDTOs[indexPrice]
    let currentInquiryNum = inquiryList[index].partDetail[indexs].inquiryNum
    wx.request({
      url: app.globalData.url + 'Goods/carAdd',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        "detailDTOList": JSON.stringify([{
          "userId": guid,
          "bizId": currentPrice.quoteDetailId,
          "inquiryId": currentCar.dataID,
          "num": currentInquiryNum ? currentInquiryNum : 1,
          "source": "cx"
        }])
      },
      success: function (response) {
        wx.hideLoading()
        let res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
        if (res.code == 1 && res.data.success) {
          let inquiryList = that.data.inquiryList
          inquiryList[index].partDetail[indexs].quoteDetailDTOs[indexPrice].quoteState = 1
          that.setData({inquiryList: inquiryList})
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.message ? res.message : '加入购物车失败，请稍后再试',
          })
        }
      }
    })
  },
  OpenService: function (event) {
    const enquiryid = event.currentTarget.dataset.id
    const provinceid = event.currentTarget.dataset.provinceid
    const cityid = event.currentTarget.dataset.cityid
    const districtid = event.currentTarget.dataset.districtid
    const guid = wx.getStorageSync('guid')
    const that = this
    service.getSettingID({
      userid: guid,
      phone: app.globalData.userInfo.phone,
      provinceid: provinceid,
      cityid: cityid,
      districtid: districtid,
      orderid: '',
      enquiryid: enquiryid,
      cartid: '',
      usertype: '',
      fromtype: 1
    }).then((result) => {
      if (result.data.code == 1) {
        let params = service.createServiceURL({
          siteid: app.globalData.SERVICE_CONFIG.siteid,
          settingid: result.data.data,
          uid: guid,
          uname: app.globalData.userInfo.phone,
          phone: app.globalData.userInfo.phone,
          typeId: 1,
          userlevel: result.data.userlevel
        })
        wx.navigateTo({url: '/pages/service/service?' + params})
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: result.data.message ? result.data.message : '获取客服组失败',
        })
      }
    }).catch((error) => {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: error,
      })
    })
  }
})