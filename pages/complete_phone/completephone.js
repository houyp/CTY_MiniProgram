// pages/complete_phone/completephone.js
const common = require('../../public/js/common.js')
const app = getApp()
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone: '',              // 补充的手机号
    code: '',               // 短信验证码
    timeStatus: false,      // 倒计时开关
    time: 60                // 倒计时时间
  },

  onLoad: function () {
    guid = wx.getStorageSync('guid') || ''
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 获取短信验证码
   */
  getSMScode: function(){
    let self = this
    let phone = self.data.phone
    if (!common.verifyPhone(phone)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请输入正确的手机号码',
      })
      return false
    }
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Login/sendSMSAction',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        'phone': phone, // 默认值
        'guid': guid
      },
      success: (res) => {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if(res.data.code === 7) {
          this.startTime();
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '短信验证码发送失败，请稍后再试',
          })
        }
      }
    })
  },

  /**
   * 开启倒计时
   */
  startTime: function(){
    let self = this
    self.setData({ 'timeStatus': true })
    let reduceTime = setInterval(function () {
      let currentTime = self.data.time
      if(currentTime !== 0) {
        let nextTime = currentTime - 1
        self.setData({ 'time': nextTime })
      } else {
        stopTime()
      }
    }, 1000)
    let stopTime = function () {
      clearInterval(reduceTime)
      self.setData({
        'timeStatus': false,
        'time': 60
      })
    }
  },

  /**
   * 发送绑定手机的请求
   */
  postCompletePhone: function () {
    let self = this
    let phone = self.data.phone
    let code = self.data.code
    if (!common.verifyPhone(phone)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请输入正确的手机号码',
      })
      return false
    }
    if (!common.verifySMSCode(code)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请输入正确的验证码',
      })
      return false
    }
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Login/CompletionMeg',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        'phone': phone,
        'code': code, // 默认值
        'guid': guid
      },
      success: (res) => {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code === 1) {
          wx.setStorageSync('guid', res.data.guid)
          wx.setStorageSync('um', res.data.um)
          wx.reLaunch({
            url: '/pages/index/index',
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '绑定手机号失败，请稍后再试',
          })
        }
      }
    })
  },

  /**
   * 设置手机
   */
  setPhone: function (e) {
    this.setData({ 'phone': e.detail.value })
  },

  /**
   * 设置验证码
   */
  setCode: function (e) {
    this.setData({ 'code': e.detail.value })
  }

})