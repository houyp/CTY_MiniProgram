const app = getApp()
const common = require('../../public/js/common.js')
const service = require('../../public/js/service')
const regeneratorRuntime = require('../../public/js/regenerator-runtime')
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userAttr: null,
    sum: null,                            // 结算总价
    balance: null,                        // 余额
    blanknote: null,                      // 白条
    useBalance: null,                     // 用了多少余额
    useBlankNote: null,                   // 用了多少白条
    orderAddress: {                       // 订单省市信息
      province: '省',                     // 省
      province_code: '',                  // 省ID
      city: '市',                         // 市
      city_code: '',                      // 市ID
      area: '区',                         // 区
      area_code: ''                       // 区ID
    },
    isAssets: false,                      // 白条状态
    pid: null,                            // 订单号
    addAddressShow: false,                // 新增收货地址开关
    addAddressData: null,                 // 新增收货地址数据
    addressList: [],                      // 收货地址列表
    addressSelect: {                      // 选择的下拉框
      isShow: false,                      // 是否显示
      chooseType: null,                   // 当前下拉显示的类型
      list: []                            // 当前下拉显示的列表
    },
    invoiceChexkBox: false,               // 发票选择框
    invoiceShow: false,                   // 发票操作框是否显示
    invoiceType: '',                      // 发票类型
    invoiceCurrent: {},                   // 当前发票
    invoiceNew: {                         // 新增发票
      invoiceType: ['个人', '单位'],       // 新增发票的类型
      invoiceTypeCur: '个人',              // 默认类型
      name: null,                         // 发票名称
      number: null                        // 发票纳税人识别号
    },
    invoiceList: [],                      // 发票列表
    balanceIS: false,                     // 是否用了余额
    blanknoteIS: false,                   // 是否用了白条
    btModelShow: false,                   // 支付密码框是否显示
    btModelValue: null,                   // 保留
    partsPrice: null,                     // 商品总价
    partsPack: null,                      // 商品钉箱费
  },

  /**
   * 初次加载
   */
  onLoad: function (options) {
    guid = wx.getStorageSync('guid') || ''
    this.setData({userAttr: app.globalData.userInfo.userAttr})
    const provinceId = options.provinceId || ''
    const cityId = options.cityId || ''
    const areaId = options.areaId || ''
    this.setData({
      'isAssets': app.globalData.userInfo.isAssets,
      'partsPrice': options.partsPrice,
      'partsPack': options.partsPack,
      'orderAddress': {
        'province': common.getAddressName(provinceId, 0),
        'province_code': provinceId,
        'city': common.getAddressName(cityId, 1),
        'city_code': cityId,
        'area': common.getAddressName(areaId, 2),
        'area_code': areaId
      }
    })
    this.getSum()
  },

  /**
   * 每次显示
   */
  onShow: function () {
    this.getOrderInfo()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 加载收货地址，发票信息，余额，白条剩余
   */
  getOrderInfo: function () {
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Order/AddressBuyersLists',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        'guid': guid
      },
      success: function (res) {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code == 1) {
          that.setData({
            'balance': res.data.data.cashback_surplus,
            'blanknote': res.data.data.lous_surplus,
            'addressList': res.data.addreses.map((item) => {
              let _Obj = {}
              Object.keys(item).forEach((key) => {
                  if (key != 'is_default') {
                      _Obj[key] = item[key]
                  } else {
                      _Obj['isChecked'] = Boolean(item[key])
                  }
              })
              return _Obj
            }),
            'invoiceList': res.data.invoice
          })
        }
      }
    })
  },

  /**
   * 添加新收货地址
   */
  addAddress: function () {
    if (this.data.addressList.length >= 10) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '收货地址最多增加10条。如需新增，请删除其他地址后进行新增',
      })
      return
    }
    this.setData({
      'addAddressShow': true,
      'addAddressData': {
        area: {
          area: '收货区',
          city: '收货市',
          province: '收货省',
          town: '收货街道',
          areaid: null,
          cityid: null,
          provinceid: null,
          townid: null,
          area_id: null,
          city_id: null,
          province_id: null,
          town_id: null
        },
        name: null,
        tel: null,
        address: null,
        companyName: null,
        remark: null,
      }
    })
  },
  /**
   * 新增地址设置数据
   * @param {Object} e 
   */
  setDataValue: function (e) {
    let dataKey = e.currentTarget.dataset.data
    let addAddressData = this.data.addAddressData
    addAddressData[dataKey] = e.detail.value
    this.setData({ 'addAddressData': addAddressData})
  },
  /**
   * 隐藏新增收货地址
   */
  hideAddAddress: function () {
    this.setData({'addAddressShow': false})
  },
  /**
   * 新增地址选择省份
   * @param {Object} e 
   */
  chooseProvince: function (e) {
    common.WXRequest('Conter/getProvince').then((result) => {
      if (result.data.code === 0) {
        this.setData({'addressSelect': {
          isShow: true,
          chooseType: 'province',
          list: this.handleAddressData(result.data.data, 'provinceName', 'provinceId')
        }})
      }
    })
  },
  /**
   * 新增地址选择城市
   */
  chooseCity: function (e) {
    let _id = this.data.addAddressData.area.provinceid
    common.WXRequest('Conter/getCity', {provinceId: _id}).then((result) => {
      if (result.data.code === 0) {
        this.setData({
          'addressSelect': {
            isShow: true,
            chooseType: 'city',
            list: this.handleAddressData(result.data.data, 'cityName', 'cityId')
          }
        })
      }
    })
  },
  /**
   * 新增地址选择城市
   */
  chooseArea: function (e) {
    let _id = this.data.addAddressData.area.cityid
    common.WXRequest('Conter/getDistrict', {cityId: _id}).then((result) => {
      if (result.data.code === 0) {
        this.setData({
          'addressSelect': {
            isShow: true,
            chooseType: 'area',
            list: this.handleAddressData(result.data.data, 'districtName', 'districtId')
          }
        })
      }
    })
  },
  /**
   * 新增地址选择街道
   */
  chooseTown: function (e) {
    let _id = this.data.addAddressData.area.areaid
    common.WXRequest('Conter/getTownList', {districtId: _id}).then((result) => {
      if (result.data.code === 0) {
        this.setData({
          'addressSelect': {
            isShow: true,
            chooseType: 'town',
            list: this.handleAddressData(result.data.data, 'townName', 'townId')
          }
        })
      }
    })
  },
  /**
   * 将省市区街道的接口数据的KEY统一
   * @param {Array} data 列表
   */
  handleAddressData: function (data, Ckey, Cvalue) {
    let addressData = []
    console.log(data)
    data.forEach((item, index) => {
      let addressObj = {}
      Object.keys(item).forEach((key) => {
        if (key == Ckey) {
          addressObj['name'] = data[index][key]
        } else if (key == Cvalue) {
          addressObj['id'] = data[index][key]
        } else {
          addressObj[key] = data[index][key]
        }
      })
      addressData.push(addressObj)
    })
    return addressData
  },
  /**
   * 关闭地址下拉
   */
  hideSelectList: function () {
    this.setData({
      'addressSelect': {
        isShow: false,
        chooseType: null,
        list: []
      }
    })
  },
  /**
   * 选择地址下拉
   */
  chooseAreaList: function (e) {
    let id = e.currentTarget.dataset.id
    let _id = e.currentTarget.dataset._id
    let name = e.currentTarget.dataset.name
    let addAddressData = this.data.addAddressData
    switch (this.data.addressSelect.chooseType) {
      case 'province':
        addAddressData.area.province = name
        addAddressData.area.provinceid = id
        addAddressData.area.province_id = _id
        addAddressData.area.city = '收货市'
        addAddressData.area.cityid = null
        addAddressData.area.city_id = null
        addAddressData.area.area = '收货区'
        addAddressData.area.areaid = null
        addAddressData.area.area_id = null
        addAddressData.area.town = '收货街道'
        addAddressData.area.townid = null
        addAddressData.area.town_id = null
        this.setData({ 'addAddressData': addAddressData})
        this.chooseCity(e)
        break
      case 'city':
        addAddressData.area.city = name
        addAddressData.area.cityid = id
        addAddressData.area.city_id = _id
        addAddressData.area.area = '收货区'
        addAddressData.area.areaid = null
        addAddressData.area.area_id = null
        addAddressData.area.town = '收货街道'
        addAddressData.area.townid = null
        addAddressData.area.town_id = null
        this.setData({ 'addAddressData': addAddressData })
        this.chooseArea(e)
        break
      case 'area':
        addAddressData.area.area = name
        addAddressData.area.areaid = id
        addAddressData.area.area_id = _id
        addAddressData.area.town = '收货街道'
        addAddressData.area.townid = null
        addAddressData.area.town_id = null
        this.setData({ 'addAddressData': addAddressData })
        this.chooseTown()
        break
      case 'town':
        addAddressData.area.town = name
        addAddressData.area.townid = id
        addAddressData.area.town_id = _id
        this.setData({ 'addAddressData': addAddressData })
        this.hideSelectList()
        break
      default:
        break
    }
  },

  /**
   * 保存收货地址
   */
  saveAddress: function () {
    if (!this.data.addAddressData.name) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请填写收货人姓名',
      })
      return
    }
    if (!this.data.addAddressData.tel || !common.verifyPhone(this.data.addAddressData.tel)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请填写正确的收货人电话',
      })
      return
    }
    if (!this.data.addAddressData.area.cityid || !this.data.addAddressData.area.provinceid || !this.data.addAddressData.area.areaid || !this.data.addAddressData.area.townid) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请选择收货人省市区街道地址',
      })
      return
    }
    if (!this.data.addAddressData.address) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请填写收货人详细地址',
      })
      return
    }
    if (!this.data.addAddressData.companyName) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请填写收货企业名称',
      })
      return
    }
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Conter/AddressBuyersAdd',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        'guid': guid,
        'company': this.data.addAddressData.companyName,
        'name': this.data.addAddressData.name,
        'tel': this.data.addAddressData.tel,
        'addr': this.data.addAddressData.address,
        'area': this.data.addAddressData.area.area,
        'city': this.data.addAddressData.area.city,
        'town': this.data.addAddressData.area.town,
        'province': this.data.addAddressData.area.province,
        'areaid': this.data.addAddressData.area.areaid,
        'cityid': this.data.addAddressData.area.cityid,
        'town_id': this.data.addAddressData.area.townid,
        'provinceid': this.data.addAddressData.area.provinceid,
        'remark': this.data.addAddressData.remark
      },
      success: function(res) {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code == 1) {
          let addressList = that.data.addressList
          addressList.push({
            id: res.data.id,
            consignee: that.data.addAddressData.name,
            phone: that.data.addAddressData.tel,
            address: that.data.addAddressData.address,
            add_phone: that.data.addAddressData.address,
            company: that.data.addAddressData.companyName,
            town: that.data.addAddressData.area.town,
            district: that.data.addAddressData.area.area,
            city: that.data.addAddressData.area.city,
            province: that.data.addAddressData.area.province,
            town_id: that.data.addAddressData.area.townid,
            district_id: that.data.addAddressData.area.areaid,
            city_id: that.data.addAddressData.area.cityid,
            province_id: that.data.addAddressData.area.provinceid,
          })
          that.setData({ 'addressList': addressList})
          that.hideAddAddress()
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '新增地址失败，请稍后再试',
          })
        }
      }
    })
  },

  /**
   * 删除收货地址
   */
  deleteAddress: function (e) {
    let that = this
    let addressID = e.currentTarget.dataset.addressid
    let index = e.currentTarget.dataset.index
    wx.showModal({
      title: '提示',
      content: '确认删除',
      success: function (res){
        if (res.confirm) {
          wx.showLoading({
            title: '数据加载中...',
            mask: true
          })
          wx.request({
            url: app.globalData.url + 'Conter/AddressBuyersDelete',
            header: {
              'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            data: {
              guid: guid,
              id: addressID
            },
            success: function (res) {
              wx.hideLoading()
              if (res.data.code == 1) {
                let addressList = that.data.addressList
                addressList.splice(index, 1)
                that.setData({ 'addressList': addressList })
              } else {
                wx.showModal({
                  title: '提示',
                  showCancel: false,
                  content: '删除地址失败，请稍后再试',
                })
              }
            }
          })
        }
      }
    })
  },

  /**
   * 选择收货地址
   */
  chooseAddress: function (e) {
    let that = this
    let index = e.currentTarget.dataset.index
    let addressList = that.data.addressList
    addressList.forEach((key, indexs) => {
      key.isChecked = index == indexs ? true : false
    })
    this.setData({ 'addressList': addressList})
  },

  /**
   * 勾选发票
   */
  chooseInvoice: function () {
    let invoiceChexkBox = this.data.invoiceChexkBox
    let invoiceShow = this.data.invoiceShow
    let invoiceType = this.data.invoiceType
    let invoiceList = this.data.invoiceList
    let balanceIS = this.data.balanceIS
    let blanknoteIS = this.data.blanknoteIS
    if (invoiceChexkBox) {
      invoiceChexkBox = false
      invoiceShow = false
    } else {
      invoiceChexkBox = true
      invoiceShow = true
      balanceIS = false
      this.setData({'balanceIS': balanceIS })
      if (this.data.partsPrice > this.data.blanknote) {
        blanknoteIS = false
      }
      this.setData({ 'blanknoteIS': blanknoteIS })
    }
    if (invoiceList.length == 0) {
      invoiceType = 'add'
    } else {
      invoiceType = 'list'
    }
    this.setData({ 'invoiceCurrent': {}, 'invoiceChexkBox': invoiceChexkBox, 'invoiceShow': invoiceShow, 'invoiceType': invoiceType})
    this.getSum()
  },

  /**
   * 隐藏发票操作
   */
  hideInvoice: function () {
    this.setData({'invoiceChexkBox': false, 'invoiceShow': false})
    this.getSum()
  },

  /**
   * 选择发票
   */
  chooseInvoiceCurrent: function(e) {
    let item = e.currentTarget.dataset.item
    this.setData({ 'invoiceCurrent': item, 'invoiceChexkBox': true, 'invoiceShow': false})
    this.getSum()
  },

  /**
   * 新增发票
   */
  addInvoice: function () {
    this.setData({ 'invoiceType': 'add' })
  },

  /**
   * 新增发票培类型
   */
  chooseInvoiceType: function (e) {
    let item = e.currentTarget.dataset.item
    let invoiceNew = this.data.invoiceNew
    invoiceNew.invoiceTypeCur = item
    this.setData({ 'invoiceNew': invoiceNew})
  },

  /**
   * 设置新发票的名称
   */
  setInvoiceName: function (e) {
    let value = e.detail.value
    let invoiceNew = this.data.invoiceNew
    invoiceNew.name = value
    this.setData({ 'invoiceNew': invoiceNew })
  },
  /**
   * 设置新发票的识别号
   */
  setInvoiceNumber: function (e) {
    let value = e.detail.value
    let invoiceNew = this.data.invoiceNew
    invoiceNew.number = value
    this.setData({ 'invoiceNew': invoiceNew })
  },
  /**
   * 验证新增的发票是否已经存在于发票列表
   * @param {String} type 发票类型
   * @param {String} name 发票名称
   * @param {Number} code 发票号
   */
  isInvoice: function (type, name, code) {
    let invoiceList = this.data.invoiceList
    let isBel = false
    invoiceList.forEach((key, index) => {
      if (type == '个人') {
        if (key.type == name) {
          isBel = true
        }
      }
      if (type == '单位') {
        if (key.name == name || key.code == code) {
          isBel = true
        }
      }
    })
    return isBel
  },
  /**
   * 保存发票
   */
  saveInvoice: function () {
    if (!this.data.invoiceNew.invoiceTypeCur) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请选择发票类型',
      })
      return
    }
    if (this.data.invoiceNew.invoiceTypeCur == '单位' && !this.data.invoiceNew.name) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请填写发票名称',
      })
      return
    }
    if (this.data.invoiceNew.invoiceTypeCur == '单位' && !this.data.invoiceNew.number) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请填写纳税人识别号',
      })
      return
    }
    if (this.isInvoice( this.data.invoiceNew.invoiceTypeCur, 
                        this.data.invoiceNew.invoiceTypeCur != '个人' ? this.data.invoiceNew.name : '个人', 
                        this.data.invoiceNew.invoiceTypeCur != '个人' ? this.data.invoiceNew.number : '' )) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请勿提交已经存在的发票信息',
      })
      return
    }
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Conter/saveInvoice',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        guid: guid, 
        invoiceTypeCur: this.data.invoiceNew.invoiceTypeCur,
        name: this.data.invoiceNew.invoiceTypeCur != '个人' ? this.data.invoiceNew.name : '个人',
        number: this.data.invoiceNew.invoiceTypeCur != '个人' ? this.data.invoiceNew.number : ''
      },
      success: function(res) {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code == 1) {
          let invoiceList = that.data.invoiceList
          let name = that.data.invoiceNew.invoiceTypeCur != '个人' ? that.data.invoiceNew.name : '个人'
          let isChange = false
          let invoiceCurrent = {}
          invoiceList.forEach((key) => {
            if (key.name == name) {
              key.code = that.data.invoiceNew.invoiceTypeCur != '个人' ? that.data.invoiceNew.number : ''
              isChange = true
              invoiceCurrent = key
            }
          })
          if (!isChange) {
            invoiceCurrent = {
              type: that.data.invoiceNew.invoiceTypeCur,
              name: that.data.invoiceNew.invoiceTypeCur != '个人' ? that.data.invoiceNew.name : '个人',
              code: that.data.invoiceNew.invoiceTypeCur != '个人' ? that.data.invoiceNew.number : ''
            }
            invoiceList.push(invoiceCurrent)
          }
          that.setData({ 
            'invoiceChexkBox': true,
            'invoiceShow': false,
            'balanceIS': false,
            'invoiceList': invoiceList,
            'invoiceCurrent': invoiceCurrent
          })
          that.getSum()
        }
      }
    })
  },

  /**
   * 勾选使用余额
   */
  chooseBalance: function () {
    let balanceIS = this.data.balanceIS
    let invoiceChexkBox = this.data.invoiceChexkBox
    let invoiceShow = this.data.invoiceShow
    let invoiceCurrent = this.data.invoiceCurrent
    let blanknoteIS = this.data.blanknoteIS
    if (!this.data.balance) {
      return
    }
    if (!balanceIS) {
      invoiceChexkBox = false
      invoiceShow = false
      invoiceCurrent = {}
    } else {
      if (this.data.partsPrice > this.data.blanknote) {
        blanknoteIS = false
      }
      this.setData({ 'blanknoteIS': blanknoteIS })
    }
    balanceIS = !balanceIS
    this.setData({ 'balanceIS': balanceIS, 'invoiceChexkBox': invoiceChexkBox, 'invoiceShow': invoiceShow, 'invoiceCurrent': invoiceCurrent })
    this.getSum()
  },

  /**
   * 勾选使用白条
   */
  chooseBlankNote: function () {
    let blanknoteIS = this.data.blanknoteIS
    blanknoteIS = !blanknoteIS
    if (this.data.partsPrice - this.data.useBalance > this.data.blanknote) {
      blanknoteIS = false
    }
    this.setData({ 'blanknoteIS': blanknoteIS })
    this.getSum()
  },

  /**
   * 计算各种价格
   */
  getSum: function () {
    let sum = this.data.sum
    let balanceIS = this.data.balanceIS
    let blanknoteIS = this.data.blanknoteIS
    let partsPrice = Number(this.data.partsPrice)
    let partsPack = Number(this.data.partsPack)
    let balance = Number(this.data.balance)
    let blanknote = Number(this.data.blanknote)
    let useBalance = Number(this.data.useBalance)
    let useBlankNote = Number(this.data.useBlankNote)
    if (balanceIS) {
      if ((partsPrice) > balance) {
        useBalance = balance
        if (blanknoteIS) {
          if ((partsPrice - useBalance) > blanknote) {
            useBlankNote = blanknote
          } else {
            useBlankNote = common.getMoney(partsPrice - useBalance)
          }
        } else {
          useBlankNote = 0
        }
      } else {
        useBalance = common.getMoney(partsPrice)
        useBlankNote = 0
      }
    } else {
      useBalance = 0
      if (blanknoteIS) {
        if ((partsPrice - useBalance) > blanknote) {
          useBlankNote = blanknote
        } else {
          useBlankNote = common.getMoney(partsPrice - useBalance)
        }
      } else {
        useBlankNote = 0
      }
    }
    sum = common.getMoney(partsPrice - useBalance - useBlankNote)
    this.setData({ 'sum': sum, 'useBalance': useBalance, 'useBlankNote': useBlankNote})
  },
  /**
   * 显示支付密码
   */
  showPayPassword: function () {
    this.setData({'btModelShow': true})
  },
  /**
   * 关闭支付密码
   */
  closePayPassword: function () {
    this.setData({ 'btModelShow': false })
  },
  /**
   * 提交支付密码
   */
  submitPayPassword: function () {
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + '',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        guid: guid
      },
      success: function (res) {
        wx.hideLoading()
        that.closePayPassword()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code == 1) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '支付成功，确认前往订单页面',
            success: function () {
              wx.reLaunch({
                url: '/pages/order/order',
              })
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '支付失败，请稍后再试'
          })
        }
      }
    })
  },

  /**
   * 收货地址和订单地址是否一致
   */
  isAddressCoincide: function (shoppingAddress) {
    return shoppingAddress.district_id == this.data.orderAddress.area_code ? true : false
  },

  /**
   * 更新订单
   */
  saveOrder: async function () {
    let that = this
    let currentAddress = {}
    this.data.addressList.forEach((key) => {
      if (key.isChecked) {
        currentAddress = key
      }
    })
    if (JSON.stringify(currentAddress) == '{}') {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请选择收货地址',
      })
      return
    }
    if (!currentAddress.town_id) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '该收货地址已失效，请删除后添加新的收货地址',
      })
      return
    }
    if (!this.isAddressCoincide(currentAddress)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请选择与配件地址一致的收货地址',
      })
      return
    }
    if (this.data.invoiceChexkBox && !this.data.invoiceCurrent.name) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请选择发票后再提交结算',
      })
      return
    }
    let data = {
      order_price: this.data.blanknoteIS ? this.data.useBlankNote : this.data.sum,
      accept_name: currentAddress.consignee,
      accept_phone: currentAddress.phone,
      company: currentAddress.company,
      address: currentAddress.address,
      fapiao: this.data.invoiceChexkBox,
      cash_back_amount: common.getMoney(this.data.useBalance),
      invoice: {
        title: this.data.invoiceCurrent.name,
        code: this.data.invoiceCurrent.code,
        is_company: this.data.invoiceCurrent.type == '单位' ? 1 : 0
      },
      orders: []
    }
    wx.showLoading({
      title: '确认订单中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Order/CreateOrders',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        guid: guid,
        data: JSON.stringify(data)
      },
      success: function (res) {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({ url: '/pages/authorization/authorization' })
        } else if (res.data.code == 1) {
          that.paySubmit(res.data.data)
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '结算失败，请稍后再试'
          })
        }
      }
    })
  },

  /**
   * 发起支付
   */
  paySubmit: async function (orderid) {
    let that = this
    wx.showLoading({
      title: '支付中...',
      mask: true
    })
    if (this.data.sum == '0.00') {
      // sum为0，走白条接口
      const res = await common.WXRequest('Pay/lousPay', {
        guid: guid,
        order_id: orderid,
        price: common.getMoney(that.data.useBlankNote)   // 使用了多少白条
      })
      wx.hideLoading()
      if (res.data.code == 0) {
        wx.reLaunch({ url: '/pages/authorization/authorization'})
      } else if (res.data.code == 1) {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: '支付成功，请前往订单查看',
          success: function () {wx.reLaunch({url: '/pages/order/order'})}
        })
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: res.data.message ? res.data.message : '支付失败，请前往订单页面继续支付',
          success: function () {wx.reLaunch({url: '/pages/order/order'})}
        })
      }
    } else {
      const res = await common.WXRequest('Pay/wxpay', {
        guid: guid,
        order_id: orderid,
        price: common.getMoney(that.data.sum) // 总计
      })
      wx.hideLoading()
      if (res.data.code == 0) {
        wx.reLaunch({ url: '/pages/authorization/authorization' })
      } else if (res.data.code == 1) {
        wx.requestPayment({
          'timeStamp': res.data.data.timeStamp,
          'nonceStr': res.data.data.nonceStr,
          'package': res.data.data.package,
          'signType': 'MD5',
          'paySign': res.data.data.paySign,
          'success': function (res) {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: '支付成功，请前往订单查看',
              success: function () { wx.reLaunch({url: '/pages/order/order'})}
            })
          },
          'fail': function (res) { 
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: '支付失败，请前往订单页面继续支付',
              success: function () {wx.reLaunch({url: '/pages/order/order'})}
            })
           },
          'complete': function (res) { }
        })
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: res.data.message ? res.data.message : '支付失败，请前往订单页面继续支付',
          success: function () {wx.reLaunch({url: '/pages/order/order'})}
        })
      }
    }
  },
  // 弹窗基础方法
  OpenService: function (event) {
	  const guid = wx.getStorageSync('guid')
    const that = this
    service.getSettingID({
      userid: guid,
      phone: app.globalData.userInfo.phone,
      provinceid: this.data.orderAddress.province_code,
      cityid: this.data.orderAddress.city_code,
      districtid: this.data.orderAddress.area_code,
      orderid: this.data.pid,
      enquiryid: '',
      cartid: '',
      usertype: '',
      fromtype: 4
    }).then((result) => {
      if (result.data.code == 1) {
        let params = service.createServiceURL({
          siteid: app.globalData.SERVICE_CONFIG.siteid,
          settingid: result.data.data,
          uid: guid,
          uname: app.globalData.userInfo.phone,
          phone: app.globalData.userInfo.phone
        })
        wx.navigateTo({url: '/pages/service/service?' + params})
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: result.data.message ? result.data.message : '获取客服组失败',
        })
      }
    }).catch((error) => {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: error,
      })
    })
  }
})