const common = require('../../public/js/common.js')
const service = require('../../public/js/service')
const regeneratorRuntime = require('../../public/js/regenerator-runtime')
const app = getApp()
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    VIN: '',                  // 查询的VIN
    model: '',                // 识别出来的公告号
    errMsg: '',                // 报错信息
  },
  /**
   * 初始化
   */
  onLoad: async function (options) {
    guid = wx.getStorageSync('guid') || ''
    if (options.imagepath) {  // 有照片路径
      this.uploadPhoto(options.imagepath)
    } else {
      if (wx.getStorageSync('Last_Search_VIN')) {
        let vin = wx.getStorageSync('Last_Search_VIN')
        this.setData({ 'VIN': vin, 'errMsg': '' })
      } else {
        const that = this
        wx.showLoading({
          title: '数据加载中...',
          mask: true
        })
        const res = await common.WXRequest('Api/getLastVin', {'guid': guid})
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code === 1) {
          that.setData({ 'VIN': res.data.data, 'errMsg': '' })
        }
      }
    }
  },
  onShow: function () {
    this.setData({ model: '', errMsg: '' })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  
  /**
   * 设置VIN
   */
  setVIN: function(e) {
    let str = e.detail.value.toUpperCase().replace(/[^a-zA-Z0-9]/ig, '')
    wx.setStorageSync('Last_Search_VIN', str)
    this.setData({ 'VIN': str})
  },
  /**
   * 手机拍照
   */
  takePhoto: function(){
    wx.navigateTo({url: '/pages/camera/camera'})
  },
  /**
   * 寻找相册
   */
  findPhoto: function(){
    let self = this
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album'],
      success: function (res) {
        let tempFilePaths = res.tempFilePaths
        self.uploadPhoto(tempFilePaths[0])
      },
    })
  },
  /**
   * 上传行驶本图片
   */
  uploadPhoto: function(file) {
    wx.showLoading({
      title: '上传图片中...',
      mask: true
    })
    let that = this
    this.setData({VIN: ''})
    wx.uploadFile({
      url: app.globalData.url + 'Api/imgIdentify',
      filePath: file,
      name: 'photo',
      success: (response) => {
        const res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
        wx.hideLoading()
        if (res.code == 0) {
          wx.reLaunch({url: '/pages/authorization/authorization'})
        } else if (res.code === 1) {
          that.setData({ 
            'VIN': res.data.vin,
            'model':res.data.model
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '识别失败，请手动键入',
          })
        }
      }
    })
  },

  /**
   * 提交查询VIN
   */
  queryVIN: function () {
    let VIN = this.data.VIN
    let that = this
    if (!common.verifyVIN(VIN)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请输入正确的VIN',
      })
      return false
    }
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Api/getVinlist',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        vin: VIN, // 默认值
        'guid': guid
      },
      method: 'POST',
      success: (res) => {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code === 1) {
          let QueryVINData = that.handleQueryVINData(res.data.data)
          wx.setStorageSync('QueryVINData', QueryVINData)
          wx.setStorageSync('Last_Search_VIN', VIN)
          let CTV_DATA_NotificationNumber = that.SoleNotificationNumber(QueryVINData.CTV_DATA.listCarModel)
          if (JSON.stringify(QueryVINData.EPC_DATA) !== '{}') {     // 如果有EPC数据，直接询价
            wx.navigateTo({ 
              url: '/pages/output_vin/outputvin',
            })
          } else {                                              // 如果没有EPC数据，走CTV
            if (CTV_DATA_NotificationNumber.size > 1) {         // CTV出现2个以上公告号
              if (Array.from(CTV_DATA_NotificationNumber).indexOf(that.data.model) < 0) {
                // 公告号集合无识别出的公告号
                wx.navigateTo({
                  url: '/pages/announcement/announcement?number='+ Array.from(CTV_DATA_NotificationNumber) +'',
                })
              } else {
                wx.navigateTo({
                  url: '/pages/output_vin/outputvin?Announcement=' + that.data.model + '',
                })
              }
            } else {
              wx.navigateTo({
                url: '/pages/output_vin/outputvin',
              })
            }
          }
        } else if (res.data.code === 4) {
          wx.setStorageSync('QueryVINData', '{}')
          wx.setStorageSync('Last_Search_VIN', VIN)
          wx.navigateTo({
            url: '/pages/output_vin/outputvin',
          })
        } else if (res.data.code === 404) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '请上传营业执照等信息，进行后续流程，点击确定进入上传界面',
            success: function (res) {
              if (res.confirm) {
                wx.reLaunch({
                  url: '/pages/enterprise_information/centerpriseinfomation',
                })
              } else if (res.cancel) {

              }
            }
          })
        } else {
          this.setData({'errMsg': res.data.message ? res.data.message : JSON.parse(res.data.data.epcVins).msg})
        }
      }
    })
  },

  /**
   * 整理响应数据
   * EPC与CTV数据拆分独立
   * 响应接口整合了CTV和EPC的数据
   * 去掉了接口中辅助的业务逻辑
   * 
   * @param {Object} data 
   */
  handleQueryVINData: function (data) {
    let CTV_DATA = JSON.parse(data.appletVin).code === 11 || JSON.parse(data.appletVin).code === 12 ? JSON.parse(data.appletVin) : {}
    let EPC_DATA = JSON.parse(data.epcVins).code === 61 ? JSON.parse(data.epcVins) : {} 
    return {
      'CTV_DATA': CTV_DATA,
      'EPC_DATA': EPC_DATA
    }
  },

  // 从返回的数据中，提取公告号内容
  SoleNotificationNumber: function (data) {
    let NotificationNumber = new Set()
    data && data.forEach((item) => {
      NotificationNumber.add(item['公告号'])
    })
    return NotificationNumber
  }
})
