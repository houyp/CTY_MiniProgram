// pages/authorization/authorization.js
const app = getApp()
const common = require('../../public/js/common.js')
const regeneratorRuntime = require('../../public/js/regenerator-runtime')
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    guid = wx.getStorageSync('guid') || ''
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.showLoading({
      title: '页面加载中...',
      mask: true
    })
    this.login()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 登录方法
   */
  login: function () {
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.code) {
          //发起网络请求
          common.WXRequest('Login/loginAction', {code: res.code}).then((res) => {
            wx.hideLoading()
            guid = res.data.guid
            wx.setStorageSync('guid', guid)
            if (res.data.code == 0) {
              wx.reLaunch({ url: '/pages/authorization/authorization' })
            } else if (res.data.code === 1) {
              wx.setStorageSync('um', res.data.um)
              wx.reLaunch({ url: '/pages/index/index' })
            } else if (res.data.code === 2) {
              wx.navigateTo({ url: '/pages/complete_phone/completephone' })
            } else {
              wx.showModal({
                title: '提示',
                showCancel: false,
                content: res.data.message ? res.data.message : '用户Code接收失败，无法进行后续操作，请稍后再试'
              })
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '登录失败！' + res.errMsg,
          })
        }
      },
      fail: () => {
        wx.showModal({
          title: '用户未授权',
          showCancel: false,
          content: '如需正常使用小程序功能，请进行授权操作。',
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        })
      }
    })
  },
  
  bindGetUserInfo: function (e) {
    if (e.detail.errMsg === "getUserInfo:ok") {
      wx.showLoading({
        title: '数据加载中...',
        mask: true
      })
      common.WXRequest('Login/DeCodeUnionid', {
        'encryptedData': e.detail.encryptedData,
        'iv': e.detail.iv,
        'guid': guid
      }).then((res) => {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({ url: '/pages/authorization/authorization' })
        } else if(res.data.code == 1) {
          guid = res.data.guid
          wx.setStorageSync('guid', guid)
          wx.setStorageSync('um', res.data.um)
          wx.reLaunch({ url: '/pages/index/index' })
        } else if (res.data.code == 2) {
          wx.navigateTo({ url: '/pages/complete_phone/completephone' })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '微信授权失败，请稍后再试',
          })
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '微信授权失败，请重试'
      })
    }
  }
})
