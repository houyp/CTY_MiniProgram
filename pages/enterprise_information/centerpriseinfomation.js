const common = require('../../public/js/common.js')
const app = getApp()
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    company: '',        // 企业名称
    tel: '',            // 电话
    card: '',           // 身份证
    ischeck: 0,         // 审核状态
    pic: ''             // 营业执照照片
  },
  
  onLoad: function(options) {
    guid = wx.getStorageSync('guid') || ''
    this.getCompanyInfo()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 获取企业信息
   */
  getCompanyInfo: function () {
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    let that = this
    wx.request({
      url: app.globalData.url + 'Conter/getInformation',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        'guid': guid
      },
      success: function (res) {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code === 1) {
          that.setData({
            'company': res.data.data.company,
            'tel': res.data.data.contact,
            'card': res.data.data.faren,
            'ischeck': res.data.data.shenhe,
            'pic': res.data.data.zhizhao
          })
        }
      }
    })
  },

  /**
   * 保存企业信息
   */
  saveCompanyInfo: function () {
    if (!this.data.company || !this.data.tel || !this.data.card || !this.data.pic) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请将信息填写完整'
      })
      return;
    }
    if (!common.verifyPhone(this.data.tel)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请填写正确的联系人电话'
      })
      return;
    }
    if (!common.verifyCardID(this.data.card)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请填写正确的身份证号码'
      })
      return;
    } 
    let data = {}
    wx.showLoading({
      title: '正在上传...',
      mask: true
    })
    if (this.data.pic.match('com')) {
      wx.request({
        url: app.globalData.url + 'Conter/companyEdit',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        data: {
          'guid': guid,
          'company': this.data.company,
          'phone': this.data.tel,
          'identityid': this.data.card
        },
        success: function (res) {
          wx.hideLoading()
          if (res.data.code == 0) {
            wx.reLaunch({
              url: '/pages/authorization/authorization',
            })
            return
          }
          if (res.data.code === 1) {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: '提交成功，确认返回首页',
              success: function (res) {
                if (res.confirm) {
                  wx.reLaunch({
                    url: '/pages/index/index',
                  })
                }
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: res.data.message ? res.data.message : '提交失败，请稍后再试',
            })
          }
        }
      })
    } else {
      wx.uploadFile({
        url: app.globalData.url + 'Conter/companyInformation',
        filePath: this.data.pic,
        name: 'imgfile',
        formData: {
          'guid': guid,
          'company': this.data.company,
          'imgfile': this.data.pic,
          'phone': this.data.tel,
          'identityid': this.data.card
        },
        success: function (res) {
          wx.hideLoading()
          if (JSON.parse(res.data).code == 0) {
            wx.reLaunch({
              url: '/pages/authorization/authorization',
            })
            return
          }
          if (JSON.parse(res.data).code === 1) {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: '提交成功，确认返回首页',
              success: function (res) {
                if (res.confirm) {
                  wx.reLaunch({
                    url: '/pages/index/index',
                  })
                }
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: JSON.parse(res.data).message ? JSON.parse(res.data).message : '提交失败，请稍后再试',
            })
          }
        },
        fail: function (res) {
          console.log(res)
        }
      })
    }
    
  },
  /**
   * 上传图片
   */
  uploadImage: function(){
    let that = this
    if (this.data.ischeck == 2) return
    wx.chooseImage({
      count: 1,
      success: function(res) {
        let filePath = res.tempFilePaths[0]
        that.setData({'pic': filePath})
      },
    })
  },
  /**
   * 设置企业名称
   */
  setDataCompanyName: function (e) {
    this.setData({'company': e.detail.value})
  },
  /**
   * 设置电话
   */
  setDataTel: function (e) {
    this.setData({ 'tel': e.detail.value })
  },
  /**
   * 设置身份证
   */
  setDataCardID: function (e) {
    this.setData({ 'card': e.detail.value })
  },
})