// pages/order/order.js
const common = require('../../public/js/common.js')
const service = require('../../public/js/service')
const regeneratorRuntime = require('../../public/js/regenerator-runtime')
const app = getApp()
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    lous_surplus: 0,              // 白条余额
    pageIndex: 1,                 // 分页页数
    pageSize: 15,                 // 分页数量
    searchStr: '',              // 搜索词
    timeArray: ['90','180','270'],
    timeArrayText: ['最近90天','最近180天','最近270天'],
    timeArrayIndex: 0,
    PayArrayText: ['微信支付', '白条支付'],
    PayArrayIndex: null,
    orderType: [{name: '订单', new: 0}, {name: '售后单', new: 0}],
    orderTypeIndex: 0,
    orderList: [],
    afterSalesList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    guid = wx.getStorageSync('guid') || ''
    // ?action=search&type=order&id=1234567890
    const action = options.action || ''
    const type = options.type || ''
    const id = options.id || ''
    switch (action) {
      case 'search':
        console.log('Params解析类型：',type, '搜索的VIN或单号：', id)
        this.setData({orderTypeIndex: (type == 'order' ? 0 : 1), searchStr: id})
        if (type == 'order') {
          this.getOrderList({
            'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
            'beginOrderTime': "",
            'endOrderTime': "",
            'vinCode': id,
            'pageIndex': 1,
            'pageSize': this.data.pageSize,
            'statusCode': "ALL",
            'userId': guid
          }, 'update')
        } else if (type == 'aftersale') {
          this.getAfterSalesList({
            'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
            'beginOrderTime': "",
            'endOrderTime': "",
            'vinCode': id,
            'pageIndex': 1,
            'pageSize': this.data.pageSize,
            'userId': guid
          }, 'update')
        }
        break
      default:
        this.getOrderList({
          'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
          'beginOrderTime': "",
          'endOrderTime': "",
          'vinCode': "",
          'pageIndex': this.data.pageIndex,
          'pageSize': this.data.pageSize,
          'statusCode': "ALL",
          'userId': guid
        }, 'update')
        break
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 初始化工作
    this.queryNum()
    this.setData({
      timeArrayIndex: 0,
      orderTypeIndex: 0
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.orderTypeIndex == 0) {
      // 订单
      this.getOrderList({
        'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': this.data.searchStr,
        'pageIndex': this.data.pageIndex + 1,
        'pageSize': this.data.pageSize,
        'statusCode': "ALL",
        'userId': guid
      }, 'append')
    } else {
      // 售后单
      this.getAfterSalesList({
        'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': this.data.searchStr,
        'pageIndex': this.data.pageIndex + 1,
        'pageSize': this.data.pageSize,
        'userId': guid
      }, 'append')
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  // 售后原因
  queryNum: async function () {
    const res = await common.WXRequest('Order/queryNum', {userId: guid})
    const waitDealNum = (res.data.code == 1 && res.data.data.code == 0 && res.data.data.model.waitDealNum) ? res.data.data.model.waitDealNum : 0
    this.setData({orderType: [{name: '订单', new: 0}, {name: '售后单', new: waitDealNum}]})
  },
  /**
   * 更改搜索词
   */
  setSearchInput: function (e) {
    this.setData({searchStr: e.detail.value.toUpperCase().replace(/[^a-zA-Z0-9]/ig, '')})
  },
  /**
   * 关闭搜索
   */
  closeSearch: function() {
    if (!this.data.searchStr) {return}
    this.setData({searchStr: null})
    if (this.data.orderTypeIndex == 0) {
      // 订单
      this.getOrderList({
        'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': "",
        'pageIndex': 1,
        'pageSize': this.data.pageSize,
        'statusCode': "ALL",
        'userId': guid
      }, 'update')
    } else {
      // 售后单
      this.getAfterSalesList({
        'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': "",
        'pageIndex': 1,
        'pageSize': this.data.pageSize,
        'userId': guid
      }, 'update')
    }
  },
  /**
   * 发起搜索
   */
  searchInquiry: function () {
    let Search = this.data.searchStr || ''
    if (!Search) {return}
    if (this.data.orderTypeIndex == 0) {
      // 订单
      this.getOrderList({
        'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': Search,
        'pageIndex': 1,
        'pageSize': this.data.pageSize,
        'statusCode': "ALL",
        'userId': guid
      }, 'update')
    } else if (this.data.orderTypeIndex == 1) {
      // 售后单
      this.getAfterSalesList({
        'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': Search,
        'pageIndex': 1,
        'pageSize': this.data.pageSize,
        'userId': guid
      }, 'update')
    }
  },
  /**
   * 筛选时间选择变更
   */
  bindPickerChange: function(e){
    this.setData({timeArrayIndex: e.detail.value})
    let Search = this.data.searchStr || ''
    if (this.data.orderTypeIndex == 0) {
      // 订单
      this.getOrderList({
        'TimeBucket': this.data.timeArray[e.detail.value],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': Search,
        'pageIndex': 1,
        'pageSize': this.data.pageSize,
        'statusCode': "ALL",
        'userId': guid
      }, 'update')
    } else if (this.data.orderTypeIndex == 1) {
      // 售后单
      this.getAfterSalesList({
        'TimeBucket': this.data.timeArray[e.detail.value],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': Search,
        'pageIndex': 1,
        'pageSize': this.data.pageSize,
        'userId': guid
      }, 'update')
    }
  },
  /**
   * 切换标签
   */
  getTab: function (e) {
    this.setData({orderTypeIndex: e.currentTarget.dataset.index, searchStr: null})
    this.queryNum()
    if (e.currentTarget.dataset.index == 0) {
      this.getOrderList({
        'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': "",
        'pageIndex': 1,
        'pageSize': this.data.pageSize,
        'statusCode': "ALL",
        'userId': guid
      }, 'update')
    } else {
      this.getAfterSalesList({
        'TimeBucket': this.data.timeArray[this.data.timeArrayIndex],
        'beginOrderTime': "",
        'endOrderTime': "",
        'vinCode': "",
        'pageIndex': 1,
        'pageSize': this.data.pageSize,
        'userId': guid
      }, 'update')
    }
  },

  /**
   * 重置时间筛选条件为初始值
   */
  initSearchTimeValue: function () {

  },

  /**
   * 请求订单列表接口
   */
  getOrderList: async function (reqData, type) {
    const that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    try {
      const res = await common.WXRequest('Order/orderList', reqData)
      if (res.data.code === 1) {
        let NewOrderList
        if (type == 'update') {
          NewOrderList = that.handleOrderListData(res.data.data.model.list)
        } else if (type == 'append') {
          NewOrderList = that.data.orderList
          if (that.handleOrderListData(res.data.data.model.list).length != 0) {
            NewOrderList = NewOrderList.concat(that.handleOrderListData(res.data.data.model.list))
          }
        }
        that.setData({orderList: NewOrderList, pageIndex: Math.ceil(NewOrderList.length/that.data.pageSize)})
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: res.data.message ? res.data.message : '获取订单列表失败，请稍后再试',
        })
      }
    } catch (err) {
      console.log(err)
    }
    wx.hideLoading()
  },
  /**
   * 整理订单列表返回的数据
   */
  handleOrderListData: function (rawOrderList) {
    return rawOrderList
  },
  /**
   * 请求售后单列表接口
   */
  getAfterSalesList: async function (reqData, type) {
    const that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    const res = await common.WXRequest('Order/salelist', reqData)
    wx.hideLoading()
    if (res.data.code === 1) {
      let NewAfterSale
      if (type == 'update') {
        NewAfterSale = that.handleAfterSalesListData(res.data.data.model.list)
      } else if (type == 'append') {
        NewAfterSale = that.data.afterSalesList
        if (that.handleAfterSalesListData(res.data.data.model.list).length != 0) {
          NewAfterSale = NewAfterSale.concat(that.handleAfterSalesListData(res.data.data.model.list))
        }
      }
      that.setData({afterSalesList: NewAfterSale, pageIndex: Math.ceil(NewAfterSale.length/that.data.pageSize)})
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: res.data.message ? res.data.message : '获取售后单列表失败，请稍后再试',
      })
    }
  },
  /**
   * 整理售后单列表返回的数据
   */
  handleAfterSalesListData: function (rawAfterSalesList) {
    return rawAfterSalesList
  },

  // -------------------  订单操作  ------------------
  /**
   * 订单 确认收货
   */
  receivingConfirm: async function (e) {
    const that = this
    const index = e.currentTarget.dataset.index
    const childindex = e.currentTarget.dataset.childindex
    const orderId = e.currentTarget.dataset.id
    const orderDetailIds = e.currentTarget.dataset.detailid
    let orderList = this.data.orderList
    wx.showLoading({
      title: '确认收货中...',
      mask: true
    })
    const res = await common.WXRequest('Order/receivingConfirm', {
      'userId': guid,
      'orderId': orderId,
      'orderDetailIds': orderDetailIds
    })
    wx.hideLoading()
    if (res.data.code === 1) {
      orderList[index].orderVO.orderDetailOpenVOs[childindex].orderDetailStatus = 50
      orderList[index].orderVO.orderDetailOpenVOs[childindex].afterSaleStatisticsVO = {
        "processingReturnNum": 0,
        "returnFinishNum": 0,
        "processingExchangeNum": 0,
        "exchangeFinishNum": 0,
        "allowReturnNum": orderList[index].orderVO.orderDetailOpenVOs[childindex].orderNum,
        "orderNum": orderList[index].orderVO.orderDetailOpenVOs[childindex].orderNum
      }
      that.setData({orderList: orderList})
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: res.data.message ? res.data.message : '确认收货成功',
      })
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: res.data.message ? res.data.message : '确认收货失败，请稍后再试',
      })
    }
  },
  /**
   * 订单 申请售后 跳转到售后申请页面
   */
  toAfterSale: function (e) {
    const orderId = e.currentTarget.dataset.id
    const orderdetailid = e.currentTarget.dataset.detailid
    wx.navigateTo({url: '/pages/after_sale/aftersale?orderid=' + orderId + '&orderdetailid=' + orderdetailid + ''})
  },
  /**
   * 取消订单
   */
  cancelOrder: async function (e) {
    const that = this
    const orderId = e.currentTarget.dataset.orderid
    const index = e.currentTarget.dataset.index
    let orderList = this.data.orderList
    wx.showLoading({title: '取消订单中', mask: true})
    const res = await common.WXRequest('Order/orderCancel', {
      'userId': guid,
      'orderId': orderId,
    })
    if (res.data.code === 1) {
      wx.hideLoading()
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: res.data.message ? res.data.message : '取消订单成功',
      })
      orderList[index].orderVO.orderStatus = 50
      orderList[index].orderVO.orderDetailOpenVOs.forEach((item, indexChild) => {
        orderList[index].orderVO.orderDetailOpenVOs[indexChild].orderDetailStatus = 60
      })
      that.setData({orderList: orderList})
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: res.data.message ? res.data.message : '取消订单失败，请稍后再试',
      })
    }
  },
  /**
   * 选择变更
   * 未完成订单 支付
   */
  PayPickerChange: async function(e){
    const index = e.currentTarget.dataset.index
    const currentOrder = this.data.orderList[index]
    this.setData({PayArrayIndex: e.detail.value})
    // 请求用户剩余的白条金额
    const resq = await common.WXRequest('Order/AddressBuyersLists', {guid: guid})
    if (resq.data.code == 1) {
      this.setData({
        lous_surplus: resq.data.data.lous_surplus
      })
    }
    // 需付金额
    let needPrice = currentOrder.orderVO.payTotalPrice - currentOrder.orderVO.cashBackPayTotalPrice
    // 根据所选的index，走支付  1：白条 0：微信
    switch (e.detail.value) {
      case '1':
        // 如果需要支付的金额大于白条余额，不予通过
        if (needPrice > resq.data.data.lous_surplus) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '您的白条账户余额不足，请选择其他支付方式'
          })
          return
        }
        // 发起支付
        wx.showLoading({title: '支付中', mask: true})
        const resL = await common.WXRequest('Pay/lousPay', {
          guid: guid,
          order_id: currentOrder.orderVO.orderId,
          price: common.getMoney(currentOrder.orderVO.cashPayTotalPrice)   // 使用了多少白条
        })
        wx.hideLoading()
        if (resL.data.code === 1) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '支付成功，点击确定刷新',
            success(resp) {
              if (resp.confirm) {wx.redirectTo({url: '/pages/order/order'})} 
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: resL.data.message ? resL.data.message : '支付失败，请稍后再试'
          })
        }
        break;
      case '0':
        // 如果需要支付的金额为0，不予通过
        if (needPrice == 0) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '需要支付金额为0，请选择白条支付'
          })
          return
        }
        // 发起支付
        wx.showLoading({title: '支付中', mask: true})
        const resW = await common.WXRequest('Pay/wxpay', {
          guid: guid,
          order_id: currentOrder.orderVO.orderId,
          price: common.getMoney(currentOrder.orderVO.cashPayTotalPrice)      // 销售实付总额：现金部分
        })
        wx.hideLoading()
        if (resW.data.code === 1) {
          wx.requestPayment({
            'timeStamp': resW.data.data.timeStamp,
            'nonceStr': resW.data.data.nonceStr,
            'package': resW.data.data.package,
            'signType': 'MD5',
            'paySign': resW.data.data.paySign,
            'success': function (res) {
              wx.showModal({
                title: '提示',
                showCancel: false,
                content: '支付成功，点击确定刷新',
                success(resp) {
                  if (resp.confirm) { wx.redirectTo({url: '/pages/order/order'})}
                }
              })
            },
            'fail': function (res) { console.log('失败信息', res) },
            'complete': function (res) { }
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '支付失败，请稍后再试'
          })
        }
        break;
      default:
        break;
    }
  },


  // -------------------  售后单操作  ------------------
  /**
   * 跳转到售后单详情
   */
  toAfterSaleDetail: function (e) {
    const returnid = e.currentTarget.dataset.returnid
    const returncode = e.currentTarget.dataset.returncode
    const returntype = e.currentTarget.dataset.returntype
    
    wx.navigateTo({url: '/pages/after_details/afterDetail?returnid=' + returnid + '&returncode=' + returncode + '&returntype=' + returntype})
  },


  // -------------------  唤起客服  ------------------
  OpenService: function (event) {
    const orderid = event.currentTarget.dataset.id
    const provinceid = event.currentTarget.dataset.provinceid
    const cityid = event.currentTarget.dataset.cityid
    const districtid = event.currentTarget.dataset.districtid || '' 
    service.getSettingID({
      userid: guid,
      phone: app.globalData.userInfo.phone,
      provinceid: provinceid,
      cityid: cityid,
      districtid: districtid,
      orderid: orderid,
      enquiryid: '',
      cartid: '',
      usertype: '',
      fromtype: 2
    }).then((result) => {
      if (result.data.code == 1) {
        let params = service.createServiceURL({
          siteid: app.globalData.SERVICE_CONFIG.siteid,
          settingid: result.data.data,
          uid: guid,
          uname: app.globalData.userInfo.phone,
          phone: app.globalData.userInfo.phone,
          typeId: 2,
          userlevel: result.data.userlevel
        })
        wx.navigateTo({url: '/pages/service/service?' + params})
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: result.data.message ? result.data.message : '获取客服组失败',
        })
      }
    }).catch((error) => {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: error,
      })
    })
  }
})