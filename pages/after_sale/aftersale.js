// after_sale/aftersale.js
const app = getApp()
const regeneratorRuntime = require('../../public/js/regenerator-runtime')
const common = require('../../public/js/common')
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderid: null,
    orderdetailid: null,
    partIndex: null,
    typeArray: ['换货', '退货'],
    typeArrayIndex: 0,
    Quantity: 1,
    maxQuantity: 5,
    reasonArray: [],
    reasonArrayRAW: [],
    reasonArrayIndex: 0,
    imageList: [],              // 上传的图片列表
    orderDetail: {} // 订单详情
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    guid = wx.getStorageSync('guid') || ''
    const orderid = options.orderid || ''
    const orderdetailid = options.orderdetailid || ''
    this.setData({orderid: orderid, orderdetailid: orderdetailid})
    this.afterSaleReason()
    this.getOrderDetail(orderdetailid)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  getOrderDetail: async function (orderdetailid) {
    wx.showLoading({title: '数据加载中...', mask: true})
    const that = this
    const res = await common.WXRequest('Order/orderDetail', {
      userId: guid,
      orderId: this.data.orderid
    })
    wx.hideLoading()
    if (res.data.code == 1) {
      let partIndex = null
      res.data.data.model.orderVO.orderDetailOpenVOs.forEach((item, index) => {
        if (item.orderDetailId == orderdetailid) {
          partIndex = index
        }
      })
      that.setData({orderDetail: res.data.data.model, maxQuantity: res.data.data.model.orderVO.orderDetailOpenVOs[partIndex].afterSaleStatisticsVO.allowReturnNum, partIndex: partIndex})
    }
  },
  // 售后原因
  afterSaleReason: async function () {
    let that = this
    const res = await common.WXRequest('Order/afterSaleReason', {})
    if (res.data.code == 1 && res.data.data.code == 0 && res.data.data.model.length > 0) {
      let NewArr = []
      res.data.data.model.forEach((item) => {
        NewArr.push(item.reasonName)
      })
      that.setData({reasonArray: NewArr, reasonArrayRAW: res.data.data.model})
    }
  },
  // 更改申请原因
  bindPickerChange: function(e){
    this.setData({reasonArrayIndex: e.detail.value})
  },
  // 更改申请服务类型
  changeType: function (e) {
    this.setData({typeArrayIndex: e.currentTarget.dataset.index})
  },
  // 更改申请数量
  changeNumber: function (e) {
    const action = e.currentTarget.dataset.action
    const maxQuantity = this.data.maxQuantity
    let Quantity = this.data.Quantity
    switch(action) {
      case 'add':
        Quantity = Math.min(Quantity+1, maxQuantity)
        break
      case 'reduce':
        Quantity = Math.max(Quantity-1, 1)
        break
    }
    this.setData({Quantity: Quantity})
  },

  /**
   * 上传图片
   */
  afterActionUpdateImage: function () {
    const that = this
    if (this.data.imageList.length >= 5) {return}
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        let tempFilePaths = res.tempFilePaths[0]
        let imageList = that.data.imageList
        imageList.push(tempFilePaths)
        that.setData({imageList: imageList})
      }
    })
  },
  /**
   * 关闭图片
   */
  closeImage: function (e) {
    const index = e.currentTarget.dataset.index
    let imageList = this.data.imageList
    imageList.splice(index, 1)
    this.setData({imageList: imageList})
  },
  //多张图片上传
  uploadImages: function (data) {
    var paths = []
    var onlyUpload = function (data) {
      var i = data.i ? data.i : 0//当前上传的哪张图片
      var success = data.success ? data.success : 0//上传成功的个数
      var fail = data.fail ? data.fail : 0//上传失败的个数
      wx.uploadFile({
        url: data.url, 
        filePath: data.path[i],
        name: 'files',//这里根据自己的实际情况改
        formData: null,//这里是上传图片时一起上传的数据
        success: (resp) => {
          const res = typeof(resp.data) == 'string' ? JSON.parse(resp.data) : resp.data
          if (res.code == '000001') {
            success++
            paths.push(res.data[0])
          } else {
            fail++
          }
          i++;
          if(i == data.path.length) { 
            data.Callback(success, fail, paths)
          } else {
            data.i = i;
            data.success = success;
            data.fail = fail;
            onlyUpload(data);
          }
        }
      })
    }
    onlyUpload(data)
  },
  /**
   * 查看原订单
   */
  toOrder: function() {
    const id = this.data.orderDetail.orderVO.orderNo
    wx.navigateTo({url: '/pages/order/order?action=search&type=order&id='+ id +''})
  },
  /**
   * 提交售后单
   */
  afterSaleSubmit: function () {
    const that = this
    if (that.data.imageList.length == 0) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请上传凭证',
      })
      return
    }
    wx.showLoading({title: '申请提交中...', mask: true})
    that.uploadImages({
      url: app.globalData.url + 'UploadCenter/uploadFile.json?action=remote&client=' + Math.ceil(Math.random() * 100000) + '',
      path: that.data.imageList,
      Callback: function (success, fail, paths) {
        if (fail > 0) {
          wx.hideLoading()
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '图片上传失败，请重新提交',
          })
          return
        }
        let Paths = ''
        paths.forEach(element => {
          Paths += '{"picUrl": "'+element+'"},'
        });
        Paths = Paths.substring(0, Paths.length - 1)
        common.WXRequest('Order/submitsale', {
          'userId': guid,
          'orderId': that.data.orderid,
          'orderDetailId': that.data.orderdetailid,
          'topOrderId': that.data.orderDetail.orderVO.orderDetailOpenVOs[that.data.partIndex].topOrderId,
          'topOrderDetailid': that.data.orderDetail.orderVO.orderDetailOpenVOs[that.data.partIndex].topOrderDetailId,
          'totalNum': that.data.orderDetail.orderVO.orderDetailOpenVOs[that.data.partIndex].orderNum,    // 下单数量
          'maxNum': that.data.maxQuantity,    // 可退数量
          'applyNum': that.data.Quantity,   // 退几个
          'returnType': that.data.typeArray[that.data.typeArrayIndex] == '换货' ? 0 : 1,
          'reasonId': that.data.reasonArrayRAW[that.data.reasonArrayIndex].reasonId,
          'source': "cx",
          'picUrls': Paths,
          'remark': '',
          'amountDTO': ''
        }).then((res) => {
          wx.hideLoading()
          if (res.data.code === 1) {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: res.data.message ? res.data.message : '申请售后成功',
              success(res) {
                if (res.confirm) {
                  wx.navigateTo({url: '/pages/order/order'})
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              } 
            })
          } else {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: res.data.message ? res.data.message : '申请售后失败，请稍后再试',
            })
          }
        })
      }
    })
  }
})