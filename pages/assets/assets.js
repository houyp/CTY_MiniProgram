// pages/assets/assets.js
const app = getApp()
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    'balance': 0,     // 返现余额
    // 'bt_consumetotal': 0,    // 最新变动
    'bt_balance': 0,      // 白条剩余额度
    'isWhite': false,     // 是否开白条
    'bt_status': 0,       // 白条状态
    'yun': 0,       // 芸豆数
    'fx_new': 0,      // 
    'fx_state': '',     // 返现状态
  },
  onLoad: function () {
    guid = wx.getStorageSync('guid') || ''
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    this.setData({isWhite: app.globalData.userInfo.isAssets})
    this.getAssetData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 请求资产数据
   */
  getAssetData: function (){
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Conter/Property',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        'guid': guid
      },
      success: function (res) {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code === 1) {
          that.setData({
            'balance': res.data.data.balance,
            // 'bt_consumetotal': res.data.data.bt_consumetotal,
            'bt_balance': res.data.data.bt_balance,
            'bt_status': res.data.data.bt_status === 1 ? true : false,
            'yun': res.data.data.yun,
            'fx_new': res.data.data.fx_new,
            'fx_state': res.data.data.state === '+' ? '' : '-'
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '请求资产数据失败，请稍后再试',
          })
        }
      }
    })
  },

  /**
   * 账户流水
   */
  toBalance: function () {
    wx.navigateTo({
      url: '/pages/balance_list/balancelist?balance=' + this.data.balance + '',
    })
  },

  /**
   * 白条
   */
  toWhite: function () {
    wx.navigateTo({
      url: '/pages/repayment/repayment',
    })
  }
})