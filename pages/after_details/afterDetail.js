// after_details/afterDetail.js
const common = require('../../public/js/common.js')
const regeneratorRuntime = require('../../public/js/regenerator-runtime')
const app = getApp()
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    returnid: null,     // 售后id
    returntype: null,    // 售后类型
    returncode: null,     // 售后单号
    afterSaleData: {},          // 售后单详情
    actionBoxStatus: null,         // 0：上传信息  1：补录运单
    imageList: [],              // 上传运单的图片列表
    windowList: [],             // 补充信息窗口列表（上传运单等）
    windowListShow: [],          // 页面中需要展示的窗口列表
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    guid = wx.getStorageSync('guid') || ''
    const returnid = options.returnid || ''
    const returntype = options.returntype || ''
    const returncode = options.returncode || ''
    this.setData({
      returnid: returnid,
      returncode: returncode,
      returntype: returntype,
    })
    this.getAfterSaleDetailData({
      userId: guid,
      returnId: returnid,
      returnType: returntype
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 售后详情查询
   */
  getAfterSaleDetailData: async function (data) {
    const that = this
    const res = await common.WXRequest('Order/saleDetailList', data)
    if (res.data.code === 1) {
      that.setData({
        afterSaleData: that.handleAfterSaleDetailData(res.data.data.model),
        actionBoxStatus: res.data.data.model.status == 102 
                        ? 0 : res.data.data.model.status == 103
                              ? 1 : null
      })
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: res.data.message ? res.data.message : '查询售后详情信息失败，请稍后再试',
      })
    }
  },
  /**
   * 处理售后详情数据
   */
  handleAfterSaleDetailData: function (AfterSaleDetailData) {
    return AfterSaleDetailData
  },

  /**
   * 运单选择或拍照操作
   */
  afterDetailActionUpdateImage: function () {
    const that = this
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        let tempFilePaths = res.tempFilePaths[0]
        let imageList = that.data.imageList
        imageList.push(tempFilePaths)
        that.setData({imageList: imageList})
      }
    })
  },
  
  //多张图片上传
  uploadImages: function (data) {
    var paths = []
    var onlyUpload = function (data) {
      var i = data.i ? data.i : 0//当前上传的哪张图片
      var success = data.success ? data.success : 0//上传成功的个数
      var fail = data.fail ? data.fail : 0//上传失败的个数
      wx.uploadFile({
        url: data.url, 
        filePath: data.path[i],
        name: 'files',//这里根据自己的实际情况改
        formData: null,//这里是上传图片时一起上传的数据
        success: (resp) => {
          const res = typeof(resp.data) == 'string' ? JSON.parse(resp.data) : resp.data
          if (res.code == '000001') {
            success++
            paths.push(res.data[0])
          } else {
            fail++
          }
          i++;
          if(i == data.path.length) { 
            data.Callback(success, fail, paths)
          } else {
            data.i = i;
            data.success = success;
            data.fail = fail;
            onlyUpload(data);
          }
        }
      })
    }
    onlyUpload(data)
  },
  /**
   * 上传运单
   */
  sendWaybillData: function () {
    const that = this
    if (that.data.imageList.length == 0) {return}
    wx.showLoading({title: '图片上传中...', mask: true})
    this.uploadImages({
      url: app.globalData.url + 'UploadCenter/uploadFile.json?action=remote&client=' + Math.ceil(Math.random() * 100000) + '',
      path: that.data.imageList,
      Callback: function (success, fail, paths) {
        if (fail > 0) {
          wx.hideLoading()
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '图片上传失败，请重新提交',
          })
          return
        }
        common.WXRequest('Order/transportUpload', {
          'guid': guid,
          "returnId": that.data.returnid,                            //退换货id
          "returnType": that.data.returntype,                         //退换货类型（0-换货 1-退货）
          "picUrls": paths,                                        //图片URL
          "autoTrans": false,                                       // 是否有运单
        }).then((result) => {
          wx.hideLoading()
          if (result.data.code == 1) {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: '上传成功，点击确认刷新页面',
              success(res) {
                if (res.confirm) {
                  that.getAfterSaleDetailData({
                    userId: guid,
                    returnId: that.data.returnid,
                    returnType: that.data.returntype
                  })
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: result.data.message ? result.data.message : '上传失败，请稍后再试'
            })
          }
        })
      }
    })
  },
  /**
   * 无运单自送
   */
  NoWaybill: function () {
    const that = this
    wx.showLoading({title: '确认中...', mask: true})
    common.WXRequest('Order/transportUpload', {
      'guid': guid,
      "returnId": that.data.returnid,                            //退换货id
      "returnType": that.data.returntype,                         //退换货类型（0-换货 1-退货）
      "picUrls": [],                                        //图片URL
      "autoTrans": true,                                       // 是否有运单
    }).then((result) => {
      wx.hideLoading()
      if (result.data.code == 1) {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: '无运单自送确认成功，点击确认刷新页面',
          success(res) {
            if (res.confirm) {
              that.getAfterSaleDetailData({
                userId: guid,
                returnId: that.data.returnid,
                returnType: that.data.returntype
              })
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: result.message ? result.message : '无运单自送确认失败，请稍后再试',
        })
      }
    })
  },
  /**
   * 补录信息
   */
  sendSupplementaryInformation: function(){
    const that = this
    if (that.data.imageList.length == 0) {return}
    wx.showLoading({title: '图片上传中...', mask: true})
    this.uploadImages({
      url: app.globalData.url + 'UploadCenter/uploadFile.json?action=remote&client=' + Math.ceil(Math.random() * 100000) + '',
      path: that.data.imageList,
      Callback: function (success, fail, paths) {
        if (fail > 0) {
          wx.hideLoading()
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '图片上传失败，请重新提交',
          })
          return
        }
        let Paths = ''
        paths.forEach(element => {
          Paths += '{"picUrl": "'+element+'"},'
        });
        Paths = Paths.substring(0, Paths.length - 1)
        common.WXRequest('Order/customerUpload', {
          'guid': guid,
          "returnId": that.data.returnid,                            //退换货id
          "returnType": that.data.returntype,                         //退换货类型（0-换货 1-退货）
          "picUrls": Paths,                                        //图片URL
        }).then((result) => {
          wx.hideLoading()
          if (result.data.code == 1) {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: '上传成功，点击确认刷新页面',
              success(res) {
                if (res.confirm) {
                  that.getAfterSaleDetailData({
                    userId: guid,
                    returnId: that.data.returnid,
                    returnType: that.data.returntype
                  })
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              showCancel: false,
              content: result.data.message ? result.data.message : '上传失败，请稍后再试'
            })
          }
        })
      }
    })
  },
  /**
   * 关闭补充信息窗口-上传运单
   */
  closeWaybillWindow: function () {

  },
  /**
   * 关闭图片
   */
  closeImage: function (e) {
    const index = e.currentTarget.dataset.index
    let imageList = this.data.imageList
    imageList.splice(index, 1)
    this.setData({imageList: imageList})
  },
  /**
   * 查看原订单
   */
  toOrder: function() {
    const id = this.data.afterSaleData.orderNo
    wx.navigateTo({url: '/pages/order/order?action=search&type=order&id='+ id +''})
  },
})