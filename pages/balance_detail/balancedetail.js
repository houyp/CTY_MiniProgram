// pages/balance_detail/balancedetail.js
const app = getApp()
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderpid: '',       // 返现详情的订单ID
    balanceList: null       // 订单ID下的小单号列表
  },

  /**
   * 生命周期函数--监听页面创建
   */
  onLoad: function (option) {
    guid = wx.getStorageSync('guid') || ''
    this.setData({ 'orderpid': option.orderpid})
    this.getBalanceDetail(this.data.orderpid)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 请求余额详情的数据
   */
  getBalanceDetail: function (pid) {
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Conter/statementDetails',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        'guid': guid,
        'pid': pid
      },
      method: 'POST',
      success: function (res) {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code === 1) {
          that.setData({'balanceList': res.data.data})
        }
      }
    })
  }
})