// pages/balance_list/balancelist.js
const app = getApp()
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    balance: 0,       // 返现余额
    balanceList: []     // 返现列表
  },

  onLoad: function (options) {
    guid = wx.getStorageSync('guid') || ''
    this.setData({ 'balance': options.balance })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getBablanceData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 请求余额列表数据
   */
  getBablanceData: function () {
    let that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Conter/accountStatement',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        'guid': guid
      },
      method: 'POST',
      success: function (res) {
        wx.hideLoading()
        if (res.data.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        }
        if (res.data.code === 1) {
          that.setData({'balanceList': res.data.data})
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.data.message ? res.data.message : '请求账户余额列表失败，请稍后重试',
          })
        }
      }
    })
  },

  /**
   * 跳转流水详情
   */
  toBalanceDetail: function (e) {
    // let orderid = e.currentTarget.dataset.orderid || ''
    // let orderpid = e.currentTarget.dataset.orderpid || ''
    // if (orderid) {
    //   wx.navigateTo({
    //     url: '/pages/balance_detail/balancedetail?orderpid=' + orderpid + '',
    //   })
    // }
  }
})