// pages/center/center.js
const app = getApp()
const service = require('../../public/js/service')
let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    guid = wx.getStorageSync('guid')
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  toIndex: function () {
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },
  toInquiry:  function () {
    wx.reLaunch({
      url: '/pages/inquiry/inquiry',
    })
  },
  toShoppingCart: function () {
    wx.reLaunch({
      url: '/pages/shopping_cart/shoppintcart',
    })
  },
  toOrder: function () {
    wx.reLaunch({
      url: '/pages/order/order',
    })
  },
  toAssets: function (){
    wx.reLaunch({
      url: '/pages/assets/assets',
    })
  },
  toEnterpriceInfomation: function () {
    wx.reLaunch({
      url: '/pages/enterprise_information/centerpriseinfomation',
    })
  },
  toChangePhone: function () {
    wx.reLaunch({
      url: '/pages/change_phone/changephone',
    })
  },
  OpenService: function (event) {
    let that = this
    service.getSettingID({
      userid: guid,
      phone: app.globalData.userInfo.phone,
      provinceid: '',
      cityid: '',
      districtid: '',
      orderid: '',
      enquiryid: '',
      cartid: '',
      usertype: '',
      fromtype: ''
    }).then((result) => {
      if (result.data.code == 1) {
        let params = service.createServiceURL({
          siteid: app.globalData.SERVICE_CONFIG.siteid,
          settingid: result.data.data,
          uid: guid,
          uname: app.globalData.userInfo.phone,
          phone: app.globalData.userInfo.phone,
          userlevel: result.data.userlevel
        })
        wx.navigateTo({url: '/pages/service/service?' + params})
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: result.data.message ? result.data.message : '获取客服组失败',
        })
      }
    }).catch((error) => {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: error,
      })
    })
  }
})