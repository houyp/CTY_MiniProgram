// pages/inquiry/inquiry.js
const common = require('../../public/js/common.js')
const app = getApp()
const service = require('../../public/js/service')
let Timer;
let intervalTime = 86400000;   // one day
let guid
let updateNumber = wx.request({
  url: 'http://',
  header: {
    'content-type': 'application/json'
  },
  success: function (res) {
    // console.log(res.data)
  }
})

Page({
  /**
   * 页面的初始数据
   */
  data: {
    userAttr: null,               // 用户类型
    pageIndex: 1,                 // 起始页码，初始化0
    pageSize: 99999999,           // 每页放几条数据
    searchStatus: false,          // 搜索状态
    searchStr: null,              // 搜索词
    selectedPartsNumber: 0,       // 已选配件数量
    selectedPartsPrice: 0,        // 已选配件金额
    submitType: false,            // 结算按钮状态
    ShoppingCartList: [
      // {          // 购物车列表
      //   vinCode: 12345678901234567, // vin
      //   allCheck: false,            // 全选
      //   carMessage: {
      //     logo: 'https://api.autocloudpro.com/Public/car_logo_images/og.png',   // 车辆LOGO
      //     brandName: "华晨宝马",   // 品牌名称
      //     carSystemId: 3,         // 车系ID
      //     carSystemName: "3系",   // 车系名称
      //     carTypeId: 328,         // 车型ID
      //     carTypeName: "328Li",   // 车型名称
      //     engine: 'E',            // 发动机  
      //     inquiryId: 12345678915618664, // 询价单ID
      //     level: "",              // 级别
      //     place: "",              // 地区
      //     transmission: "AT",     // 变速器
      //     year: "2015",           // 年款
      //     engineDisplacement: ''  // 发动机排量
      //   },
      //   addressMessage: {
      //     province: '山西省',
      //     provinceCode: '011000',
      //     city: '太原市',
      //     cityCode: '011010',
      //     area: '迎泽区',
      //     areaCode: '011011'
      //   },
      //   partsList: [{
      //     check: false,             // 选择 def: false
      //     isShow: true,             // 是否显示 (当前时间-失效时间戳>intervalTime && partFailure)
      //     partFailure: false,       // 配件失效状态 (当前时间>失效时间戳)  false未过期 true已过期
      //     partName: '前保险杠',      // 配件名称
      //     partOE: '1235253',        // 配件OE
      //     partType: '原厂',         // 配件类型
      //     partRemark: '原品/墨西哥产/马来西亚进/要预定30天/含装箱费', // 配件描述
      //     partUnitPrice: 198.8,       // 单价
      //     partNumber: 1,            // 数量
      //     partSubtotal: 198.8,        // 小计(含钉箱费) partUnitPrice * partNumber
      //     nailBoxPrice:0,           //钉箱费     nailBoxPrice * partNumber    // 新增 by:Chris
      //     orderPrice:324             //商品总价  不含钉箱费                    // 新增 by:Chris
      //     nowTime: 1536904527417,     // 当前时间戳
      //     partDeadline: '1536904527417',  // 失效时间戳
      //     partDeadlineDate: '',     // 失效时间中文形式 通过deadlineDate函数
      //   }]
      // }
    ]
  },
  onLoad: function (options) {
    this.setData({userAttr: app.globalData.userInfo.userAttr})
    guid = wx.getStorageSync('guid') || ''
  },
  onShow: function () {
    this.setData({
      searchStatus: false,
      searchStr: null,  
      selectedPartsNumber: 0,
      selectedPartsPrice: 0,
      submitType: false,
    })
    this.getShoppingCartData({
      pageIndex: 1,
      pageSize: this.data.pageSize,
      userId: guid
    }, 'update')
  },
  onReady: function () {
    this.updatePageDeadline()
  },

  /**
   * 用户点击右上角分享x
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },
  
  onUnload: function () {
    clearInterval(Timer)
  },
  /**
   * 获得搜索框焦点
   */
  focusSearchInput: function (e) {
    this.setData({ searchStatus: true})
  },

  /**
   * 关闭搜索
   */
  closeSearch: function() {
    this.setData({ searchStatus: false, searchStr: null })
    this.getShoppingCartData({
      pageIndex: 1,
      pageSize: this.data.pageSize,
      userId: guid
    }, 'update')
  },

  /**
   * 请求购物车数据
   */
  getShoppingCartData: function (datas, handleType) {
    let that = this
    wx.request({
      url: app.globalData.url + 'Goods/carList',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: datas,
      success: (response) => {
        let res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
        if (res.code === 1 && !res.data.empty) {
          let ShoppingCartList = this.data.ShoppingCartList
          switch (handleType) {
            case 'append':
              // 数据追加
              ShoppingCartList = ShoppingCartList.concat(that.handleDataToPage(res.data.model))
              break;
            case 'update':
              // 数据更新
              ShoppingCartList = that.handleDataToPage(res.data.model)
              break;
            default:
              wx.showModal({
                title: '提示',
                showCancel: false,
                content: '请求购物车数据缺少handleType参数',
              })
              break;
          }
          that.setData({ShoppingCartList:ShoppingCartList, pageIndex: !(ShoppingCartList.length % 5) ? ShoppingCartList.length / 5 : parseInt(ShoppingCartList.length / 5) + 1})
        } else if (res.code === 1 && res.data.empty) {
          switch (handleType) {
            case 'append':
              // 数据追加
              break;
            case 'update':
              // 数据更新
              that.setData({ShoppingCartList: [], pageIndex: 1})
              break;
            default:
              wx.showModal({
                title: '提示',
                showCancel: false,
                content: '请求购物车数据缺少handleType参数',
              })
              break;
          }
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.message ? res.message : '购物车数据加载失败，请稍后再试',
          })
        }
      }
    })
  },
  /**
   * 将接口数据处理为页面所需数据格式, important!!!
   */
  handleDataToPage: function (datas) {
    let pageData = []
    datas.forEach((item, index) => {
      let isShowCar = false
      let CartObj = {
        vinCode: item.carTypeDTO.vinCode,
        allCheck: false,
        isShowCar: true,
        partsList: [],
        addressMessage: {
          province: item.carTypeDTO.provinceName,
          provinceCode: item.carTypeDTO.provinceId,
          city: item.carTypeDTO.cityName,
          cityCode: item.carTypeDTO.cityId,
          area: item.carTypeDTO.districtName,
          areaCode: item.carTypeDTO.districtId
        },
        carMessage: {
          logo: item.carTypeDTO.logoUrl,
          brandId: item.carTypeDTO.brandId,
          brandName: item.carTypeDTO.brandName,
          carSystemId: item.carTypeDTO.carSystemId,
          carSystemName: item.carTypeDTO.carSystemName,
          carTypeId: item.carTypeDTO.carTypeId,
          carTypeName: item.carTypeDTO.carTypeName,
          engine: item.carTypeDTO.engine,
          inquiryId: null,
          level: null,
          place: null,
          transmission: item.carTypeDTO.transmission,
          year: item.carTypeDTO.year,
          engineDisplacement: item.carTypeDTO.engineDisplacement,
          engineDescribe: item.carTypeDTO.engineDescribe
        }
      }
      item.detailDTOList.forEach((items, indexs) => {
        let CartPartsObj = {
          check: false,
          isShow: !(item.carTypeDTO.nowTime > items.deadline && item.carTypeDTO.nowTime - items.deadline > intervalTime),
          partFailure: item.carTypeDTO.nowTime > items.deadline,
          partName: items.partsName,
          partOE:  items.partsCode,
          partType: items.factoryType,
          partTypeName: items.factoryType == 1 ? '原厂' : '品牌',
          nailBoxPrice: items.nailBoxPrice,
          partsBrandName:  items.partsBrandName,
          partsEpcRemarks: items.partsEpcRemarks,
          partRemark: items.remark,
          num: items.num,
          partUnitPrice: items.price,
          partSubtotal: Number(items.totalPrice) + Number(items.nailBoxPrice * items.num),
          orderPrice: items.totalPrice,
          nowTime: item.carTypeDTO.nowTime,
          partNumber: items.num ? items.num : '1',
          partDeadline: items.deadline,
          partDeadlineDate: '',
          address: items.address,
          bizId: items.bizId,
          bizType: items.bizType,
          sku:items.sku,
          cartId: items.cartId,
          provinceId: items.provinceId,
          cityId: items.cityId,
          districtId: items.districtId,
          garageId: items.garageId,
          invoiceType: items.invoiceType,
          placeOfOrigin: items.placeOfOrigin,
          insuranceNo: items.insuranceNo,
          plateNo: items.plateNo,
          source: items.source,
          status: items.status,
          supplierId: items.supplierId,
          supplierName: items.supplierName,
          inquiryDetailId: items.inquiryDetailId,
          inquiryId: items.inquiryId,
          inquiryNo: items.inquiryNo,
          inquiryUserId: items.inquiryUserId,
          bookOrder: items.bookOrder === 1 ? true : false,
          bookOrderDays: items.bookOrderDays || 0,
          bookAllowReturn: items.bookAllowReturn ===  1 ? '不可' : '可'
        }
        if (CartPartsObj.isShow) {
          isShowCar = true
        }
        CartObj.partsList.push(CartPartsObj)
        CartObj.isShowCar = isShowCar
      }) 
      CartObj.partsList.sort(function(a, b) {
        return (a.partDeadline - a.nowTime) < (b.partDeadline - b.nowTime)
      })
      pageData.push(CartObj)
    })
    return pageData
  },
  /**
   * 将未来的时间戳输出倒计时文本
   */
  deadlineDate: function (deadline, currentTime) {
    let currentDate = currentTime
    let differ = deadline > currentDate ? deadline - currentDate : 0
    let day = parseInt(differ / 86400000)
    let hour = parseInt((differ - 86400000 * day) / 3600000)
    let minute = parseInt((differ - 86400000 * day - 3600000 * hour) / 60000)
    let second = parseInt((differ - 86400000 * day - 3600000 * hour - 60000 * minute) / 1000)
    if (differ < 60000) {
      return `${second}s后失效`
    } else if (differ < 3600000) {
      return `${minute}min后失效`
    } else if (differ < 86400000) {
      return `${hour}h后失效`
    } else {
      return `${day}天后失效`
    }
  },
  /**
   * 发起搜索
   */
  searchInquiry: function () {
    const that = this
    if (!common.verifyVIN(that.data.searchStr)) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请输入正确的VIN',
      })
      return
    }
    this.getShoppingCartData({
      pageIndex: 1,
      pageSize: that.data.pageSize,
      userId: guid,
      vinCode: that.data.searchStr
    }, 'update')
  },
  /**
   * 更改搜索词
   */
  setSearchInput: function (e) {
    this.setData({searchStr: e.detail.value.toUpperCase().replace(/[^a-zA-Z0-9]/ig, '')})
  },
  /**
   * 发起新询价跳转
   */
  toNewInquiry: function () {
    wx.reLaunch({
      url: '/pages/input_vin/inputvin',
    })
  },
  /**
   * 配件的全选
   */
  checkAllParts: function (e) {
    let index = e.currentTarget.dataset.index
    let ShoppingCartList = this.data.ShoppingCartList
    if (ShoppingCartList[index].allCheck) {
      ShoppingCartList[index].allCheck = false
      ShoppingCartList[index].partsList.forEach((item,indexs) => {
        ShoppingCartList[index].partsList[indexs].check = false
      })
    } else {
      let isHasCheck = false
      ShoppingCartList[index].partsList.forEach((item,indexs) => {
        if (!item.partFailure) {
          isHasCheck = true
          ShoppingCartList[index].partsList[indexs].check = true
        }
      })
      if (isHasCheck) { ShoppingCartList[index].allCheck = true }
    }
    this.setData({ShoppingCartList: ShoppingCartList})
    this.getSum()
  },
  /**
   * 配件的选择
   */
  checkPart: function (e) {
    let index = e.currentTarget.dataset.index
    let indexs = e.currentTarget.dataset.indexs
    let ShoppingCartList = this.data.ShoppingCartList
    let RawAllCheck = true
    ShoppingCartList[index].partsList[indexs].check = !ShoppingCartList[index].partsList[indexs].check
    ShoppingCartList[index].partsList.forEach((Part) => {
      if (!Part.check) {
        RawAllCheck = false
      }
    })
    ShoppingCartList[index].allCheck = RawAllCheck ? true : false
    this.setData({ShoppingCartList:ShoppingCartList})
    this.getSum()
  },
  /**
   * 数量加
   */
  addNumber: function (e) {
    if (this.data.userAttr == 3 || this.data.userAttr == 5) {return}
    let index = e.currentTarget.dataset.index
    let indexs = e.currentTarget.dataset.indexs
    let ShoppingCartList = this.data.ShoppingCartList
    let item = ShoppingCartList[index].partsList[indexs]
    let cartID = e.currentTarget.dataset.cartid
    let oldPartNumber = item.partNumber
    item.partNumber += 1
    item.partSubtotal = Number(item.partNumber * item.partUnitPrice) + Number(item.nailBoxPrice * item.partNumber)    // 更改 by:Chris
    item.orderPrice = item.partNumber * item.partUnitPrice,                      // 新增  by:Chris
    this.partNumberChange(cartID, item.partNumber, oldPartNumber, index, indexs)
    this.setData({ShoppingCartList: ShoppingCartList})
    this.getSum()
  },
  /**
   * 数量减
   */
  reduceNumber: function (e) {
    if (this.data.userAttr == 3 || this.data.userAttr == 5) {return}
    let index = e.currentTarget.dataset.index
    let indexs = e.currentTarget.dataset.indexs
    let ShoppingCartList = this.data.ShoppingCartList
    let item = ShoppingCartList[index].partsList[indexs]
    let cartID = e.currentTarget.dataset.cartid
    let oldPartNumber = item.partNumber
    item.partNumber = Math.max(1, item.partNumber - 1)
    item.partSubtotal = Number(item.partNumber * item.partUnitPrice) + Number(item.nailBoxPrice * item.partNumber),   // 更改 by:Chris
    item.orderPrice = item.partNumber * item.partUnitPrice,                      // 新增  by:Chris
    this.partNumberChange(cartID, item.partNumber, oldPartNumber, index, indexs)
    this.setData({ShoppingCartList: ShoppingCartList})
    this.getSum()
  },
  /**
   * 将数量变化同步给后台
   */
  partNumberChange: function (cartID, num, oldPartNumber, index, indexs) {
    let that = this
    updateNumber.abort()
    updateNumber = wx.request({
      url: app.globalData.url + 'Goods/carUpdate',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        "cartId": cartID,
        "num": num
      },
      success: (response) => {
        let res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
        if (res.code === 1 && res.data.success) {

        } else {
          let ShoppingCartList = that.data.ShoppingCartList
          let item = ShoppingCartList[index].partsList[indexs]
          item.partNumber = oldPartNumber
          that.setData({ShoppingCartList: ShoppingCartList})
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.message ? res.message : '数量更新失败，请稍后再试',
          })
        }
      }
    })
  },
  /**
   * 计算并更新选中配件个数和总计
   */
  getSum: function () {
    let ShoppingCartList = this.data.ShoppingCartList
    let submitType = this.data.submitType
    let allNumber = 0
    let allSum = 0
    let address = ''  // 地址
    let vin = ''  // vin
    let isSubmitType = true
    ShoppingCartList.forEach((item, index) => {
      item.partsList.forEach((items, indexs) => {
        if (items.check) {
          if (!address) {
            address = item.addressMessage.areaCode
          } else {
            if (item.addressMessage.areaCode !== address) {
              isSubmitType = false
            }
          }
          if (!vin) {
            vin = item.vinCode
          } else {
            if (item.vinCode !== vin) {
              isSubmitType = false
            }
          }
          allNumber += 1
          allSum = common.accAdd(allSum, items.partSubtotal)
        }
      })
    })
    submitType = allNumber > 0 && isSubmitType ? true : false
    this.setData({
      selectedPartsNumber: allNumber, 
      selectedPartsPrice: allSum, 
      submitType: submitType
    })
  },
  
  /**
   * 定时器，用来更新页面失效时间倒计时的显示
   */
  updatePageDeadline: function () {
    let that = this
    const interim = function () {
      let ShoppingCartList = that.data.ShoppingCartList
      ShoppingCartList.forEach((item, index) => {
        item.partsList.forEach((items, indexs) => {
          ShoppingCartList[index].partsList[indexs].partDeadlineDate = that.deadlineDate(items.partDeadline, items.nowTime)
          if (parseInt(items.partDeadline / 1000) - parseInt(items.nowTime / 1000) == 0) {
            that.getShoppingCartData({
              pageIndex: that.data.pageIndex,
              pageSize: that.data.pageSize,
              userId: guid
            }, 'update')
          }
          items.nowTime += 1000
        })
      })
      that.setData({ShoppingCartList: ShoppingCartList})
    }
    interim()
    Timer = setInterval(function() {
      interim()
    }.bind(that), 1000)
  },
  /**
   * 检测过期时间有没有超出当前时间
   */
  detectionDate: function (deadline, currentTime) {
    return deadline <= currentTime ? true : false
  },
  /**
   * 提交订单
   */
  submitOrder: function () {
    const um = wx.getStorageSync('um') || ''
    let partsPrice = 0      // 配件总价
    let nailBoxPrice = 0      // 钉箱费总价
    this.handleSubmitOrderData().orders[0].list.forEach((part) => {
      partsPrice = common.accAdd(partsPrice, common.accMul(part.num, common.accAdd(part.pack, part.price)))     // 总价，含钉箱费 数量 * (单价 + 钉箱费)
      nailBoxPrice = common.accAdd(nailBoxPrice, common.accMul(part.pack, part.num))                            // 总钉箱费  数量 * 钉箱费
    })
    if (!this.handleSubmitOrderData().orders[0].areaId) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '提交的配件收货地址信息不全，请重新询价',
      })
      return
    }
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    wx.request({
      url: app.globalData.url + 'Order/submitOrders',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        guid: guid,
        um: um,
        data: JSON.stringify(this.handleSubmitOrderData())
      },
      success: (response) => {
        wx.hideLoading()
        let res = (typeof response.data) === 'string' ? JSON.parse(response.data) : response.data
        if (res.code == 0) {
          wx.reLaunch({
            url: '/pages/authorization/authorization',
          })
          return
        } else if (res.code == 1) {
          wx.navigateTo({
            url: '/pages/clearing/clearing?partsPrice='+ partsPrice +'&nailBoxPrice=' + nailBoxPrice + '',
          })
        } else {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: res.message ? res.message : '订单提交失败，请稍后再试',
          })
        }
      }
    })
  },
  // 整理提交订单的数据
  handleSubmitOrderData: function () {
    const ShoppingCartList = this.data.ShoppingCartList
    let data = {
      order_price: this.data.selectedPartsPrice,  // 含钉箱费
      orders: [{
        order_price: 0,
        provinceId: null,
        cityId: null,
        areaId: null,
        source_user_id: null,   // 发布询价的USERID
        ins_number: null,       // 保单号
        plate_number: null,     // 车牌号
        list: []
      }]
    }
    ShoppingCartList.forEach((car) => {
      car.partsList.forEach((part) => {
        if (part.check) {
          data.orders[0].order_price = common.accAdd(data.orders[0].order_price, part.orderPrice)  // 不含钉箱费
          data.orders[0].provinceId = car.addressMessage.provinceCode
          data.orders[0].cityId = car.addressMessage.cityCode
          data.orders[0].areaId = car.addressMessage.areaCode
          data.orders[0].source_user_id = part.inquiryUserId || ''
          data.orders[0].ins_number = part.insuranceNo || ''
          data.orders[0].plate_number = part.plateNo || ''
          // 已选中的配件
          let partObj = {
            pack: part.nailBoxPrice,    // 更改 by:Chris
            brandId: car.carMessage.brandId,
            brandName: car.carMessage.brandName,
            year: car.carMessage.year,
            inquiry_id: part.bizType == 2 ? part.inquiryId : 0,  //更改 by:Chris
            partsCode: part.partOE,
            partsName: part.partName,
            note: part.partRemark,
            num: part.partNumber,
            bizId: part.bizId,
            price: part.partUnitPrice,   // 不含钉箱费 单价
            vinCode: car.vinCode,
            carSystemId: car.carMessage.carSystemId,
            carSystemName: car.carMessage.carSystemName,
            brandLogo: car.carMessage.logo,
            company_name: car.carMessage.brandName,
            skuId: part.bizType == 1 ? part.sku : 0,      //更改 by:Chris
            inquiryDetailId: part.inquiryDetailId.toString(),
            factoryType: part.partType,
            supplierId: part.supplierId,
            cartId:part.cartId,
            partsBrandName:part.partsBrandName,
            engineDisplacement: car.carMessage.engineDisplacement 
          }
          data.orders[0].list.push(partObj)
        }
      })
    })
    return data
  },
  OpenService: function (event) {
    const partslist = event.currentTarget.dataset.partslist
    let cartid = []
    partslist.forEach((item) => {
      cartid.push(item.cartId)
    })
    const provinceid = event.currentTarget.dataset.provinceid
    const cityid = event.currentTarget.dataset.cityid
    const districtid = event.currentTarget.dataset.districtid
    const guid = wx.getStorageSync('guid')
    const that = this
    service.getSettingID({
      userid: guid,
      phone: app.globalData.userInfo.phone,
      provinceid: provinceid,
      cityid: cityid,
      districtid: districtid,
      orderid: '',
      enquiryid: '',
      cartid: cartid,
      usertype: '',
      fromtype: 1       // 原为3
    }).then((result) => {
      if (result.data.code == 1) {
        let params = service.createServiceURL({
          siteid: app.globalData.SERVICE_CONFIG.siteid,
          settingid: result.data.data,
          uid: guid,
          uname: app.globalData.userInfo.phone,
          phone: app.globalData.userInfo.phone,
          typeId: 1,       // 原为3
          userlevel: result.data.userlevel
        })
        wx.navigateTo({url: '/pages/service/service?' + params})
      } else {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: result.data.message ? result.data.message : '获取客服组失败',
        })
      }
    }).catch((error) => {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: error,
      })
    })
  }
})