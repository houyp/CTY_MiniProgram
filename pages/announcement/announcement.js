// pages/announcement/announcement.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentAnnouncement: null,
    announcementList: []
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let params = options.number.split(',')
    params.push('无法找到')
    this.setData({announcementList: params})
  },
  // 选择公告号
  chooseAnnouncement: function (e) {
    let index = e.currentTarget.dataset.index
    this.setData({currentAnnouncement: index})
  },

  // 提交
  submitAnnouncement: function () {
    let currentAnnouncement = this.data.currentAnnouncement
    let announcementList = this.data.announcementList
    let Announcement = ''
    if (announcementList.length !== currentAnnouncement + 1) {
      Announcement = announcementList[currentAnnouncement]
    }
    wx.navigateTo({
      url: '/pages/output_vin/outputvin?Announcement=' + Announcement + '',
    })
  }
})