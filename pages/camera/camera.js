// pages/camera/camera.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noAuthorization: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    wx.authorize({
      scope: 'scope.camera',
      success(res) {
        console.log('相机已授权，可正常使用')
      },
      fail() {
        that.setData({noAuthorization: true})
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: '未授权相机，功能将无法使用，点击进行授权',
          success: function () {
            wx.openSetting({
              success: function(res) {
              }
            })
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  takePhoto() {
    // wx.navigateTo({url: '/pages/input_vin/inputvin?imagepath=wx:file://tmp_ec1f62925ebc85ca5fd7082bb062ed26d2ad2c9fc3d85e2a.jpg'})
    const that = this
    const ctx = wx.createCameraContext()
    ctx.takePhoto({
      quality: 'high',
      success: (resImagePath) => {
        wx.showModal({
          title: '提示',
          content: '拍摄成功，点击确认之后将进行识别',
          success(res) {
            console.log(resImagePath.tempImagePath)
            if (res.confirm) {
              wx.reLaunch({url: '/pages/input_vin/inputvin?imagepath=' + resImagePath.tempImagePath + ''})
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },
  error(e) {
    console.log(e.detail)
  }
})