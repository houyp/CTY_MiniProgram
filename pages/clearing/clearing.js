// pages/clearing/clearing.js
const app = getApp()
const common = require('../../public/js/common')
const regeneratorRuntime = require('../../public/js/regenerator-runtime')

let guid

Page({

  /**
   * 页面的初始数据
   */
  data: {
    partsPrice: 0,     // 结算总价
    nailBoxPrice: 0,  // 钉箱费总价
    orderData: {}     // 订单数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const partsPrice = options.partsPrice || 0
    const nailBoxPrice = options.nailBoxPrice || 0
    guid = wx.getStorageSync('guid') || ''
    this.setData({partsPrice: partsPrice, nailBoxPrice: nailBoxPrice})
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getOrderMsg()
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },

  /**
   * 获取订单信息
   */
  getOrderMsg: async function () {
    const that = this
    wx.showLoading({
      title: '数据加载中...',
      mask: true
    })
    const res = await common.WXRequest('Order/getUnPayed', {
      guid: guid
    })
    wx.hideLoading()
    if (res.data.code == 0) {
      wx.reLaunch({url: '/pages/authorization/authorization'})
      return
    }
    if (res.data.code === 1) {
      that.setData({'orderData': res.data.data})
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '获取订单信息失败，确认返回购物车',
        success: function (res) {
          if (res.confirm) { wx.reLaunch({url: '/pages/shopping_cart/shoppintcart'}) }
        }
      })
    }
  },

  /**
   * 前往补充订单信息
   */
  toEnterpriseInfo: function () {
    if (JSON.stringify(this.data.orderData) != '{}' && this.data.partsPrice != 0) {
      wx.navigateTo({
        url: '/pages/complete_order_information/completeorderinfomation?partsPrice=' + this.data.partsPrice + 
                                                                      '&provinceId=' + this.data.orderData.orders[0].provinceId + 
                                                                      '&cityId=' + this.data.orderData.orders[0].cityId + 
                                                                      '&areaId=' + this.data.orderData.orders[0].areaId + 
                                                                      '&partsPack=' + this.data.nailBoxPrice + ''
      })
    }
  },

  /**
   * 返回购物车
   */
  backShoppingCart: function () {
    wx.navigateBack({
      delta: 1
    })
  }
})