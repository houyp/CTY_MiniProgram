// pages/assembly_drawing/ assemblydrawing.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scale: 1,
    imgSwiper: {
      isShow: true,                                 // 装配图轮播
      imgUrls: [],                                   // 轮播图路径列表
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (!options.imagearray) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '图片数据缺失，请关闭重新查看',
      })
      return
    } else {
      let imgUrls = []
      JSON.parse(options.imagearray).forEach((item) => {
        imgUrls.push({url: decodeURIComponent(item.url)})
      })
      this.setData({
        imgSwiper: {
          isShow: true,
          imgUrls: imgUrls, 
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  
  /**
   * 关闭轮播
   */
  closeImgSwiper: function(){
    wx.navigateBack({delta: 1})
  },
  /**
   * 选中轮播
   */
  // chooseImgSwiper: function(){
  //   let subscript = this.data.imgSwiper.subscript
  //   this.chooseOE(undefined, subscript[0], subscript[1])
  //   this.closeImgSwiper()
  // },
})