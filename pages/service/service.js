// pages/service/service.js
const app = getApp()

Page({
  /**
   * 页面的初始数据
   */
  data: {
    url: ''
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '车通云 · 配件采购专家',
      path: 'pages/authorization/authorization'
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setURL(options)
  },
  setURL: function (options) {
    let arg = ''
    Object.keys(options).forEach((item) => {
      arg += ('' + item + '=' + options[item] + '&')
    })
    arg = arg.substring(0, arg.length - 1)
    this.setData({
      url: encodeURI(app.globalData.url + 'serviceChat.html?' + arg)
    })
  }

})