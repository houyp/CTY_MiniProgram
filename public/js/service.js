const app = getApp()
const service = {
  // 获取组ID(siteid)
  // serviceData = {
	//	userid: '',
	//	phone: '',
	//	provinceid: '',
	//	cityid: '',
	//	districtid: '',
	//	orderid: '',
  //	enquiryid: '',
  //  cartid:'',
  //  usertype:'',
  //  fromtype:' //询价单 'INQUIRY' => "1", //订单 'ORDER' => "2", //购物车 'CART' => "3",  //结算 'SETTLEMENT' => "4",
	// }
  getSettingID: function (serviceData) {
    return new Promise(function(resolve, reject){
      wx.request({
        url: app.globalData.url + 'Conter/service', 
        header: {'content-type': 'application/x-www-form-urlencoded'},
        method: 'POST',
        data: {'data': JSON.stringify(serviceData)},
        success(res) {
          resolve(res)
        },
        fail(err) {
          reject(err)
        }
      })
    })
  },
  // 生成客服弹窗的URL
  createServiceURL: function (paramObj) {
    paramObj['pageTime'] = new Date().getTime()   // 加上时间戳，解决IOS严重的缓存问题
    let arg = ''
    Object.keys(paramObj).forEach((item) => {
      arg += ('' + item + '=' + paramObj[item] + '&')
    })
    arg = arg.substring(0, arg.length - 1)
    return arg
  }
}

module.exports = service