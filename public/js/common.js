const address = require('../../public/js/newaddress.js')
const app = getApp()
const common = {
  /**
   * 验证手机号码正确性
   */
  verifyPhone: function (phone) {
    return /^1(3|4|5|6|7|8|9)\d{9}$/.test(phone)
  },
  /**
   * 验证验证码正确性
   */
  verifySMSCode: function (code) {
    return /^\d{6}$/.test(code)
  },
  /**
   * 验证身份证正确性
   */
  verifyCardID: function (id) {
    return /^[1-9](\d{16}|\d{13})[0-9xX]$/.test(id)
  },
  /**
   * 验证VIN的正确性
   */
  verifyVIN: function (VIN) {
    return /^[A-Za-z0-9]{17}$/.test(VIN)
  },
  /**
   * 获取所有省份的信息
   */
  getProvinceList: function () {
    return address.default.filter((item) => {
      return item.level == 0
    })
  },
  /**
   * 获取所有市的信息
   */
  getCityList: function (ProvinceCode) {
    let CityList = []
    address.default.forEach((key) => {
      if (key.parent == ProvinceCode && key.level == '1') {
        CityList.push(key)
      }
    })
    return CityList || []
  },
  /**
   * 获取所有区的信息
   */
  getAreaList: function (CityCode) {
    let AreaList = []
    address.default.forEach((key) => {
      if (key.parent == CityCode && key.level == '2') {
        AreaList.push(key)
      }
    })
    return AreaList || []
  },
  /**
   * 通过ID获取地址名称
   */
  getAddressName: function (code, type) {
    var Name = ''
    address.default.forEach( function(item) {
      if (item.id == code && item.level == type) {
        Name = item.name
      } 
    })
    return Name
  },
  /**
   * 验证是否是纯数字
   */
  isNumber: function (value) {
    return /^[0-9]*$/.test(value)
  },
  /**
   * 计算成价格形式，处理浮点数
   */
  getMoney: function (value) {
    var f = Math.round(value * 100) / 100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
      rs = s.length;
      s += '.';
    }
    while (s.length <= rs + 2) {
      s += '0';
    }
    return s;
  },
  /**
   * 函数节流
   * @param {*} fn 
   * @param {*} gapTime 
   */
  throttle: function(fn, gapTime) {
    if (gapTime == null || gapTime == undefined) {
        gapTime = 1000
    }
    let _lastTime = null
    return function () {
      let _nowTime = + new Date()
      if (_nowTime - _lastTime > gapTime || !_lastTime) {
        fn.apply(this, arguments)   //将this和参数传给原函数
        _lastTime = _nowTime
      }
    }
  },
  /**
   * 将时间戳转化为时间格式 'yyyy-MM-dd hh:mm:ss'
   * @param {Number} time 时间戳
   */
  formatDate: function (time){
    var date = new Date(time)
    var year = date.getFullYear(),
        month = date.getMonth() + 1, //月份是从0开始的
        day = date.getDate(),
        hour = date.getHours(),
        min = date.getMinutes(),
        sec = date.getSeconds()
    var newTime = year + '-' +
                (month < 10? '0' + month : month) + '-' +
                (day < 10? '0' + day : day) + ' ' +
                (hour < 10? '0' + hour : hour) + ':' +
                (min < 10? '0' + min : min) + ':' +
                (sec < 10? '0' + sec : sec)
    return newTime
  },
  /**
   * 封装的微信请求
   * @param {String} URL 请求地址
   * @param {Object} WXRequestData 请求数据
   * @param {Object} Params 配置信息
   */
  WXRequest: function (URL, WXRequestData, Params = {}) {
    const Header = Params.header || {'content-type': 'application/x-www-form-urlencoded'}
    const Method = Params.method || 'POST'
    return new Promise(function(resolve, reject) {
      wx.request({
        url: app.globalData.url + URL,
        header: Header,
        data: WXRequestData,
        method: Method,
        success: (res) => {
          resolve(res)
        },
        fail: (error) => {
          reject(error)
        }
      })
    }) 
  },
  /*
  函数，加法函数，用来得到精确的加法结果  
  说明：javascript的加法结果会有误差，在两个浮点数相加的时候会比较明显。这个函数返回较为精确的加法结果。
  参数：arg1：第一个加数；arg2第二个加数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数）
  调用：accAdd(arg1,arg2,d)  
  返回值：两数相加的结果
  */
 accAdd: function (arg1, arg2) {
    arg1 = arg1.toString(), arg2 = arg2.toString();
    var arg1Arr = arg1.split("."), arg2Arr = arg2.split("."), d1 = arg1Arr.length == 2 ? arg1Arr[1] : "", d2 = arg2Arr.length == 2 ? arg2Arr[1] : "";
    var maxLen = Math.max(d1.length, d2.length);
    var m = Math.pow(10, maxLen);
    var result = Number(((arg1 * m + arg2 * m) / m).toFixed(maxLen));
    var d = arguments[2];
    return typeof d === "number" ? Number((result).toFixed(d)) : result;
  },
  /*
  函数：减法函数，用来得到精确的减法结果  
  说明：函数返回较为精确的减法结果。 
  参数：arg1：第一个加数；arg2第二个加数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数
  调用：accSub(arg1,arg2)  
  返回值：两数相减的结果
  */
  accSub: function (arg1, arg2) {
    return accAdd(arg1, -Number(arg2), arguments[2]);
  },


  //除法函数，用来得到精确的除法结果
  //说明：javascript的除法结果会有误差，在两个浮点数相除的时候会比较明显。这个函数返回较为精确的除法结果。
  //调用：accDiv(arg1,arg2)
  //返回值：arg1除以arg2的精确结果
  accDiv: function (arg1, arg2) {
    var t1 = 0, t2 = 0, r1, r2;
    try { t1 = arg1.toString().split(".")[1].length } catch (e) { }
    try { t2 = arg2.toString().split(".")[1].length } catch (e) { }
    // with (Math) {
    //     r1 = Number(arg1.toString().replace(".", ""))
    //     r2 = Number(arg2.toString().replace(".", ""))
    //     return (r1 / r2) * pow(10, t2 - t1);
    // }
    r1 = Number(arg1.toString().replace(".", ""))
    r2 = Number(arg2.toString().replace(".", ""))
    return (r1 / r2) * Math.pow(10, t2 - t1);
  },


  //乘法函数，用来得到精确的乘法结果
  //说明：javascript的乘法结果会有误差，在两个浮点数相乘的时候会比较明显。这个函数返回较为精确的乘法结果。
  //调用：accMul(arg1,arg2)
  //返回值：arg1乘以 arg2的精确结果
  accMul: function (arg1, arg2) {
    var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
    try { m += s1.split(".")[1].length } catch (e) { }
    try { m += s2.split(".")[1].length } catch (e) { }
    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)
  }
}

module.exports = common