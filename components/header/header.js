// pages/header.js
const app = getApp()

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    headimgurl: '',
    phone: '',
    canIUse: wx.canIUse('open-data.type.userNickName')
  },

  /**
   * 生命周期attached
   */
  attached: function () {
    this.setData({
      'headimgurl': app.globalData.userInfo.headimgurl,
      'phone': app.globalData.userInfo.phone
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    toPageCenter: function() {
      if (getCurrentPages()[getCurrentPages().length - 1].route !== 'pages/center/center') {
        wx.navigateTo({
          url: '/pages/center/center',
        })
      }
    }
  },
})
